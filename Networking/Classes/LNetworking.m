//
//  LNetworking.m
//  Writeability
//
//  Created by Ryan on 5/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNetworking.h"

#import "LNConstants.h"

#import "LNSession_Private.h"

#import "LNBusManager.h"

#import "LNFileManager.h"

#import <Utilities/LMacros.h>

#import <Utilities/NSObject+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Global constants
#pragma mark -
//*********************************************************************************************************************//




StringConst(kLNetworkingFoundSessionNotification);
StringConst(kLNetworkingLostSessionNotification);

StringConst(kLNetworkingUpdatedFileAnnouncementNotification);
StringConst(kLNetworkingFinishedFileDownloadNotification);
StringConst(kLNetworkingAbortedFileDownloadNotification);

StringConst(kLNetworkingSessionWasHostedNotification);
StringConst(kLNetworkingSessionWasJoinedNotification);
StringConst(kLNetworkingSessionWasSelectedNotification);
StringConst(kLNetworkingSessionWasLeftNotification);




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LNetworking
#pragma mark -
//*********************************************************************************************************************//




@interface LNetworking () <LNBusManagerDelegate, LNFileManagerDelegate>

@property (nonatomic, readonly  , strong) NSMutableArray    *sessions;
@property (nonatomic, readwrite , strong) LNSession         *session;

@property (nonatomic, readonly  , strong) LNBusManager      *busManager;
@property (nonatomic, readonly  , strong) LNFileManager     *fileManager;

@end

@implementation LNetworking

#pragma mark - Static Objects
//*********************************************************************************************************************//

static NSString *const kLNetworkingDomainName               = @"org.lookee.networking_v" @quote(kLNetworkingVersion);

static NSString *const kLNetworkingFileDomainName           = @"org.lookee.fileServer_v" @quote(kLNetworkingVersion);

static NSString *const kLNetworkingNotifierPath             = @"/notifier";

static NSString *const kLNetworkingNotificationSignalName   = @"!notification";


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (LNetworking *)sharedInstance
{
    static dispatch_once_t  predicate   = 0;
    static id               instance    = nil;

    dispatch_once(&predicate, ^{
        instance = [[self alloc] __init];
    });

    return instance;
}

+ (void)start
{
    [self sharedInstance];
}

+ (LNSession *)createSessionWithName:(NSString *)name fileID:(NSString *)fileID
{
    id instance = [self sharedInstance];

    id manager  = [instance busManager];

    [instance setSession:
     [LNSession createSessionWithManager:manager metadata:
      (@{ @"ID"     : [manager peerName],
          @"name"   : [name copy],
          @"fileID" : [fileID copy] })]];

    return [instance session];
}

+ (LNSession *)createSessionWithAnnouncement:(NSString *)announcement
{
    id instance = [self sharedInstance];

    id manager  = [instance busManager];

    return [LNSession createSessionWithManager:manager announcement:announcement];
}

+ (NSArray *)files
{
    return [[self sharedInstance] fileManager].files;
}

+ (NSArray *)sessions
{
    return [self sharedInstance].sessions;
}

+ (LNSession *)session
{
    return [self sharedInstance].session;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)init __ILLEGALMETHOD__;

- (id)__init
{
    if ((self = [super init])) {
        _busManager     = [[LNBusManager alloc] initWithDomainName:kLNetworkingDomainName];

        _fileManager    = [[LNFileManager alloc] initWithDomainName:kLNetworkingFileDomainName];

        _sessions       = [NSMutableArray new];

        [self.busManager addDelegate:self];
        [self.fileManager addDelegate:self];

        [self startMonitoringSessions];
    }

    return self;
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)startMonitoringSessions
{
    @weakify();

    [self observerForNotificationKey:kLNetworkingSessionWasSelectedNotification]
    (^(NSNotification *notification) {
        @strongify();

        LNSession *session = [notification object];

        if ([session type] != kLNSessionTypeInvalid) {
            [self setSession:session];
        }
    });

    [self observerForNotificationKey:kLNetworkingSessionWasHostedNotification]
    (^(NSNotification *notification) {
        @strongify();

        LNSession *session = [notification object];

        if (![self.sessions containsObject:session]) {
            [self.sessions addObject:session];
        }
    });

    [self observerForNotificationKey:kLNetworkingSessionWasLeftNotification]
    (^(NSNotification *notification) {
        @strongify();

        LNSession *session = [notification object];

        if ([self session] == session) {
            [self setSession:nil];
        }

        if ([session type] == kLNSessionTypeInvalid) {
            [self.sessions removeObject:session];
        }
    });
}


#pragma mark - LNBusManagerDelegate
//*********************************************************************************************************************//

- (void)busManager:(LNBusManager *)manager didAddSessionNamed:(NSString *)sessionName
{
    @weakify();

    [$mainqueue addOperationWithBlock:^{
        @strongify();

        NSUInteger index
        =
        [self.sessions indexOfObjectPassingTest:^(LNSession *session, NSUInteger index, BOOL *stop) {
            return (*stop = [session.announcement isEqualToString:sessionName]);
        }];

        if (index == NSNotFound) {
            LNSession *session = [$this createSessionWithAnnouncement:sessionName];

            [self.sessions addObject:session];

            [$notifier postNotificationName:kLNetworkingFoundSessionNotification object:session];

            DBGInformation(@"Found hosted session %@", session);
        }
    }];
}

- (void)busManager:(LNBusManager *)manager didRemoveSessionNamed:(NSString *)sessionName
{
    @weakify();
    
    [$mainqueue addOperationWithBlock:^{
        @strongify();

        NSUInteger index
        =
        [self.sessions indexOfObjectPassingTest:^(LNSession *session, NSUInteger index, BOOL *stop) {
            return (*stop = [session.announcement isEqualToString:sessionName]);
        }];

        if (index != NSNotFound) {
            LNSession *session = [self sessions][ index ];

            [self.sessions removeObjectAtIndex:index];

            [$notifier postNotificationName:kLNetworkingLostSessionNotification object:session];
            
            DBGInformation(@"Lost hosted session %@", session);
        }
    }];
}


#pragma mark - LNFileManagerDelegate
//*********************************************************************************************************************//

- (void)fileManager:(LNFileManager *)manager didUpdateFromOldFiles:(NSArray *)files
{
    [$notifier postNotificationName:kLNetworkingUpdatedFileAnnouncementNotification object:files];
}

- (void)fileManager:(LNFileManager *)manager didDownloadFile:(LNFile *)file
{
    [$notifier postNotificationName:kLNetworkingFinishedFileDownloadNotification object:file];
}

- (void)fileManager:(LNFileManager *)manager didFailToDownloadFile:(LNFile *)file
{
    [$notifier postNotificationName:kLNetworkingAbortedFileDownloadNotification object:file];
}

@end