//
//  LNUser.m
//  Writeability
//
//  Created by Ryan on 5/25/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNUser_Private.h"
#import "LRemoteList_Private.h"

#import "LNBusManager.h"

#import <Utilities/LMacros.h>
#import <Utilities/LSurrogateProxy.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Node Description
#pragma mark -
//*********************************************************************************************************************//




@interface LNUserNode : LNNode

@property (nonatomic, assign, getter=isLocked)  BOOL locked;

@property (nonatomic, assign) LNUserPermissions permissions;

@property (nonatomic, copy  ) NSString          *name;

@end

@implementation LNUserNode

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LNUser
#pragma mark -
//*********************************************************************************************************************//




@interface LNUser ()
{
@protected
    LRemoteList *_datapoints;
}

@end

@implementation LNUser $remote(LNUserNode)

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)userWithSession:(LNRemote *)session ID:(NSString *)ID
{
    return [[self alloc] initWithSession:session ID:ID];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)initWithSession:(LNRemote *)session ID:(NSString *)ID
{
    LNNode *node = [[LNUserNode alloc] initWithParent:session->_node ID:ID];

    if ((self = [super initWithNode:node ID:ID])) {

    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, node: %@, id: %@>",
            $classname,
            self,
            self.node,
            self.ID];
}


#pragma mark - Properties
//*********************************************************************************************************************//

@synthesize locked      = _locked;
@synthesize permissions = _permissions;

@synthesize name        = _name;

- (NSString *)name
{
    if (!_name) {
        [self.node fetchPropertyValue:@"name" fromPeerNamed:self.ID];
        _name = [self.node name];
    }

    return _name;
}

@synthesize datapoints  = _datapoints;

- (LRemoteList *)datapoints
{ // TODO: Implement serialization of LNNodes, so they can be fetched as properties
    if (!_datapoints) {
        _datapoints = [LSurrogateProxy proxyWithType:[LRemoteList class]];

        @weakify();
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            @strongify();

            [self.node fetchChildren:^(NSArray *children) {
                for (LNNode *node in children) {
                    [self.datapoints $setObject:[LRemoteList listWithNode:node]];
                    break;
                }
            }];
        });
    }

    return _datapoints;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LSeat
#pragma mark -
//*********************************************************************************************************************//




@implementation LNSelf

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)selfWithSession:(LNRemote *)session
{
    return [[self alloc] initWithSession:session];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)initWithSession:(LNRemote *)session
{
    NSString    *ID     = [session->_node.manager peerName];

    LNNode      *node   = [[LNUserNode alloc] initWithParent:session->_node ID:ID];

    if ((self = [super initWithNode:node ID:ID])) {
        _datapoints = [LRemoteArray arrayWithAncestor:self];

        [self initializeNode];
    }

    return self;
}

- (void)initializeNode
{
    [self.node setName:[UIDevice currentDevice].name];
}

@end