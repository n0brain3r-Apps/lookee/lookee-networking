//
//  LNFile_Private.h
//  Writeability
//
//  Created by Ryan on 5/24/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNFile.h"



@class LNFileManager;



@interface LNFile ()

@property (atomic, readwrite    , assign, getter = isOwned          ) BOOL owned;
@property (atomic, readwrite    , assign, getter = isTransferring   ) BOOL transferring;

@property (nonatomic, readonly  , weak  ) LNFileManager *manager;

@property (nonatomic, readwrite , strong) NSOrderedSet  *ownerIDs;


+ (instancetype)fileWithManager:(LNFileManager *)manager name:(NSString *)name sum:(NSData *)sum size:(NSUInteger)size;

@end
