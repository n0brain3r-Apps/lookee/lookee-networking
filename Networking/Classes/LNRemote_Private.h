//
//  LRemote_Private.h
//  Writeability
//
//  Created by Ryan on 6/18/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNRemote.h"

#import "LNNode_Private.h"



@interface LNRemote ()
{
@public
    LNNode *_node;
}

- (id)initWithNode:(LNNode *)node;
- (id)initWithNode:(LNNode *)node ID:(NSString *)ID;

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Utility Macros
#pragma mark -
//*********************************************************************************************************************//




#define $remote(Type)                               \
+ (Class    )nodeClass  { return [Type class];  }   \
- (Type *   )node       { return (Type *)_node; }