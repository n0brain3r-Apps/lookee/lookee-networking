//
//  LNFileManager.h
//  Writeability
//
//  Created by Ryan on 8/15/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>



@class      LNFile;

@protocol   LNFileManagerDelegate;



@interface LNFileManager : NSObject

@property (nonatomic, readwrite , copy  ) NSString  *savePath;

@property (nonatomic, readonly  , strong) NSArray   *files;

@property (nonatomic, readonly  , strong) NSString  *domainName;


- (id)initWithDomainName:(NSString *)domainName;


- (void)addDelegate:(id<LNFileManagerDelegate>)delegate;
- (void)removeDelegate:(id<LNFileManagerDelegate>)delegate;

- (void)manageFilesAtPath:(NSString *)path;
- (void)stopManagingFilesAtPath:(NSString *)path;

- (void)
downloadFile:(LNFile *      )file
withProgress:(void(^)(float))progress
completion  :(void(^)(BOOL) )completion;

- (void)cancelUploadOfFile:(LNFile *)file;
- (void)pauseDownloadOfFile:(LNFile *)file;
- (void)cancelDownloadOfFile:(LNFile *)file;

@end


@protocol LNFileManagerDelegate <NSObject>
@optional

- (void)fileManager:(LNFileManager *)manager didUpdateFromOldFiles:(NSArray *)files;
- (void)fileManager:(LNFileManager *)manager didDownloadFile:(LNFile *)file;
- (void)fileManager:(LNFileManager *)manager didFailToDownloadFile:(LNFile *)file;

@end
