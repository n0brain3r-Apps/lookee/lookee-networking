//
//  LNSession_Private.h
//  Writeability
//
//  Created by Ryan on 5/9/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNSession.h"

#import "LNRemote_Private.h"



@class LNBusManager;



@interface LNSession ()

+ (instancetype)createSessionWithManager:(LNBusManager *)manager metadata:(NSDictionary *)metadata;
+ (instancetype)createSessionWithManager:(LNBusManager *)manager announcement:(NSString *)announcement;

@end
