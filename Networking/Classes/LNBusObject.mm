//
//  LNBusObject.m
//  AllJoynTest
//
//  Created by Ryan on 5/20/14.
//  Copyright (c) 2014 lookee. All rights reserved.
//


#import "LNBusObject_Private.h"

#import "AllJoynFramework/AJNBusObjectImpl.h"

#import "AllJoynFramework/AJNSignalHandlerImpl.h"

#import "AllJoynFramework/AJNBusAttachment.h"

#import "AllJoynFramework/AJNInterfaceDescription.h"

#import "AllJoynFramework/AJNMessageArgument.h"

#import <Utilities/LMacros.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/NSThread+Block.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Global Constants
#pragma mark -
//*********************************************************************************************************************//




static const char *const kLNBusObjectSignalMemberName = "__signal";
static const char *const kLNBusObjectMethodMemberName = "__method";




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@protocol LNBusObject <AJNBusObject, AJNSignalHandler>
@required

- (NSString *)encodedValueForKey:(NSString *)key;
- (void)setEncodedValue:(NSString *)value forKey:(NSString *)key;

- (void)receiveSignal:(NSString *)signal fromPeerNamed:(NSString *)peerName;

- (NSString *)invokeMethodString:(NSString *)methodString;

@end


class LNBusObjectImpl : AJNBusObjectImpl
{
private:
    ajn::BusAttachment &busAttachment;

public:
    LNBusObjectImpl(ajn::BusAttachment &busAttachment, const char *path, id<LNBusObject> delegate)
    : AJNBusObjectImpl(busAttachment, path, delegate), busAttachment(busAttachment)
    {

    }

    ~LNBusObjectImpl() {
        busAttachment.UnregisterAllHandlers(this);
    }

    QStatus AddInterfaceWithDescription(ajn::InterfaceDescription &interfaceDescription)
    {
        QStatus status = AddInterface(interfaceDescription);

        if (status == ER_OK) {
            const ajn::InterfaceDescription::Member *signalMember = interfaceDescription.GetMember(kLNBusObjectSignalMemberName);

            if (!signalMember) {
                QStatus status = interfaceDescription.AddSignal(kLNBusObjectSignalMemberName, "s", "argument");

                if (status == ER_OK) {
                    signalMember = interfaceDescription.GetMember(kLNBusObjectSignalMemberName);
                }
            }

            busAttachment.RegisterSignalHandler
            (this,
             static_cast<MessageReceiver::SignalHandler>
             (&LNBusObjectImpl::SignalHandler),
             signalMember,
             GetPath());

            const ajn::InterfaceDescription::Member *methodMember = interfaceDescription.GetMember(kLNBusObjectMethodMemberName);

            if (!methodMember) {
                QStatus status = interfaceDescription.AddMethod(kLNBusObjectMethodMemberName, "s", "s", "argument");

                if (status == ER_OK) {
                    methodMember = interfaceDescription.GetMember(kLNBusObjectMethodMemberName);
                }
            }

            AddMethodHandler
            (methodMember,
             static_cast<MessageReceiver::MethodHandler>
             (&LNBusObjectImpl::MethodHandler));

            interfaceDescription.Activate();
        }

        return status;
    }

    QStatus SendSignal(const char                               *peerName,
                       ajn::SessionId                           sessionID,
                       const ajn::InterfaceDescription          &interfaceDescription,
                       const char                               *message                = NULL,
                       size_t                                   messageLength           = 0,
                       uint16_t                                 timeToLive              = 0,
                       uint8_t                                  flags                   = 0)
    {
        const ajn::InterfaceDescription::Member *signalMember = interfaceDescription.GetMember(kLNBusObjectSignalMemberName);

        ajn::MsgArg argument;
        argument.typeId         = ajn::ALLJOYN_STRING;
        argument.v_string.str   = strdup(message);
        argument.v_string.len   = messageLength;

        // !!!: For sessionless signals (sessionID == 0), time-to-live is in seconds; it is in milliseconds otherwise
        return
        Signal
        (peerName,
         sessionID,
         *signalMember,
         &argument,
         1,
         (sessionID? timeToLive: timeToLive /1000),
         flags);
    }

protected:
    QStatus Get(const char *interface, const char *property, ajn::MsgArg &argument)
    {
        @autoreleasepool {
            NSString *value = [(id<LNBusObject>)delegate encodedValueForKey:@(property)];

            argument.typeId         = ajn::ALLJOYN_STRING;
            argument.v_string.str   = strdup([value UTF8String]);
            argument.v_string.len   = [value length];

            return ER_OK;
        }

        return ER_BUS_NO_SUCH_PROPERTY;
    }

    QStatus Set(const char *interface, const char *property, ajn::MsgArg &argument)
    {
        if (argument.typeId == ajn::ALLJOYN_STRING) {
            char *value = NULL; argument.Get("s", &value);

            @autoreleasepool {
                [(id<LNBusObject>)delegate setEncodedValue:@(value) forKey:@(property)];
            }

            return ER_OK;
        }

        return ER_BUS_NO_SUCH_PROPERTY;
    }

private:
    void SignalHandler(const ajn::InterfaceDescription::Member *member, const char *sourcePath, ajn::Message &message)
    {
        @autoreleasepool {
            const ajn::MsgArg *argument = message->GetArg(0);

            char *value = NULL; argument->Get("s", &value);

            [(id<LNBusObject>)delegate receiveSignal:@(value) fromPeerNamed:@(message->GetSender())];
        }
    }

    void MethodHandler(const ajn::InterfaceDescription::Member *member, ajn::Message &message)
    {
        @autoreleasepool {
            const ajn::MsgArg *argument = message->GetArg(0);

            char *value = NULL; argument->Get("s", &value);

            NSString *result;

            @try {
                result = [(id<LNBusObject>)delegate invokeMethodString:@(value)];

                ajn::MsgArg response;

                response.typeId = ajn::ALLJOYN_STRING;

                if (result) {
                    response.v_string.str = strdup([result UTF8String]);
                    response.v_string.len = [result length];
                } else {
                    response.v_string.str = NULL;
                    response.v_string.len = 0;
                }

                MethodReply(message, &response, 1);
            }
            @catch (NSError *error) {
                MethodReply(message, [error.domain UTF8String], [error.description UTF8String]);
            }
        }
    }
};




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LNBusObject
#pragma mark -
//*********************************************************************************************************************//




@interface LNBusObject () <LNBusObject>
@end

@implementation LNBusObject

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@private
    NSMutableDictionary *_interfaces;

    NSString *__path;
}


#pragma mark - Abstract Methods
//*********************************************************************************************************************//

- (NSString *)encodedValueForKey:(NSString *)key __ABSTRACTMETHOD__;
- (void)setEncodedValue:(NSString *)value forKey:(NSString *)key __ABSTRACTMETHOD__;

- (void)receiveSignal:(NSString *)signal fromPeerNamed:(NSString *)peerName __ABSTRACTMETHOD__;

- (NSString *)invokeMethodString:(NSString *)methodString __ABSTRACTMETHOD__;


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)initWithPath:(NSString *)path
{
    if ((self = [self initWithBusAttachment:nil onPath:path])) {

    }

    return self;
}

- (id)initWithBusAttachment:(AJNBusAttachment *)busAttachment onPath:(NSString *)path
{
    DBGParameterAssert([path hasPrefix:@"/"]);

    if ((self = [super initWithBusAttachment:busAttachment onPath:path])) {
        __path      = [path copy];

        _interfaces = [NSMutableDictionary new];
    }

    return self;
}

- (void)dealloc
{
    if ([self handle]) {
        delete static_cast<LNBusObjectImpl *>(self.handle);
    }
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p>",
            $classname,
            self];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)initializeInterfacesWithNames:(NSArray *)interfaceNames forBusAttachment:(AJNBusAttachment *)busAttachment
{
    DBGParameterAssert(busAttachment != nil);

    [self setHandle:(new LNBusObjectImpl(*((ajn::BusAttachment *)[busAttachment handle]), [__path UTF8String], self))];

    for (NSString *interfaceName in interfaceNames) {
        AJNInterfaceDescription *interface  = [busAttachment interfaceWithName:interfaceName];

        NSArray                 *components = [interfaceName componentsSeparatedByString:@"."];

        DBGAssert([components count] != 0);

        if (!interface) {
            interface           = [busAttachment createInterfaceWithName:interfaceName];

            Class   type        = NSClassFromString([components lastObject]);

            NSArray *encodable  = [type propertyNames].allObjects;

            DBGInformation(@"Created interface '%@' with properties <%@>",
                           [interface name], [encodable componentsJoinedByString:@","]);

            if (![interface hasProperties]) {
                if ([encodable count]) {
                    for (NSString *property in encodable) {
                        [interface
                         addPropertyWithName:property
                         signature          :@"s"
                         accessPermissions  :kAJNInterfacePropertyAccessReadWriteFlag];
                    }
                }
            }
        }

        [self addInterface:interface];

        @synchronized(_interfaces) {
            [_interfaces setObject:interface forKey:interfaceName];
        }
    }
}

- (void)
sendSignal      :(NSString *    )signal
toPeerNamed     :(NSString *    )peerName
inSessionWithID :(AJNSessionId  )sessionID
onInterfaceNamed:(NSString *    )interfaceName
{
    [self
     sendSignal         :signal
     toPeerNamed        :peerName
     inSessionWithID    :sessionID
     onInterfaceNamed   :interfaceName
     withFlags          :0];
}

- (void)
sendSignal      :(NSString *    )signal
toPeerNamed     :(NSString *    )peerName
inSessionWithID :(AJNSessionId  )sessionID
onInterfaceNamed:(NSString *    )interfaceName
withFlags       :(AJNMessageFlag)flags
{
    AJNInterfaceDescription *interface = [_interfaces objectForKey:interfaceName];

    QStatus status
    =
    static_cast<LNBusObjectImpl *>(self.handle)
    ->
    SendSignal
    ([peerName UTF8String],
     sessionID,
     *((ajn::InterfaceDescription *)interface.handle),
     [signal UTF8String],
     [signal length],
     3000,
     flags);

    if (status != ER_OK) {
        DBGWarning(@"Failed to send signal '%@' to '%@' (%s)", signal, peerName, QCC_StatusText(status));
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (QStatus)addInterface:(AJNInterfaceDescription *)interface
{
    return
    static_cast<LNBusObjectImpl *>(self.handle)
    ->
    AddInterfaceWithDescription(*((ajn::InterfaceDescription *)interface.handle));
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Category Methods
#pragma mark -
//*********************************************************************************************************************//




@implementation LNBusObject (Properties)

- (NSDictionary *)interfaces
{
    @synchronized(_interfaces) {
        return [_interfaces copy];
    }
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark AJNProxyBusObject (LNBusObjectProxy)
#pragma mark -
//*********************************************************************************************************************//





@implementation AJNProxyBusObject (LNBusObjectProxy)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (NSDictionary *)encodedValuesForPropertiesForInterfaceNamed:(NSString *)interfaceName
{
    DBGParameterAssert(interfaceName != nil);

    AJNMessageArgument  *arguments; [self propertyValues:&arguments ofInterfaceWithName:interfaceName];

    if ([arguments type] == kAJNTypeArray) {
        NSMutableDictionary *encodedValues = [NSMutableDictionary new];

        size_t      count;
        ajn::MsgArg *entries;

        [arguments value:@"a{sv}", &count, &entries];

        for (size_t index = 0; index < count; ++ index) {
            char *keyReference, *valueReference;

            entries[index].Get("{ss}", &keyReference, &valueReference);

            NSString *key   = @(keyReference);
            NSString *value = @(valueReference);

            if (key && value) {
                encodedValues[key] = value;
            } else {
                DBGWarning(@"Invalid key-value pair {%@:%@}", key, value);
            }
        }

        return [encodedValues copy];
    } else {
        DBGWarning(@"Failed to fetch property values for %@", interfaceName);
    }

    return nil;
}

- (NSString *)encodedValueOfProperty:(NSString *)property forInterfaceNamed:(NSString *)interfaceName
{
    DBGParameterAssert(interfaceName != nil);

    AJNMessageArgument *argument = [self propertyWithName:property forInterfaceWithName:interfaceName];

    char *value = NULL; [argument value:@"s", &value];

    if (value) {
        return @(value);
    } else {
        DBGWarning(@"Failed to fetch value at %@.%@", interfaceName, property);
    }

    return nil;
}

- (void)
setEncodedValue     :(NSString *    )value
ofProperty          :(NSString *    )property
forInterfaceNamed   :(NSString *    )interfaceName
{
    AJNMessageArgument *argument = [AJNMessageArgument new]; [argument setValue:@"s", value.UTF8String];

    [self
     setPropertyWithName    :property
     forInterfaceWithName   :interfaceName
     toValue                :argument
     completionDelegate     :nil
     context                :NULL
     timeout                :3000];
}

- (void)
invokeMethodString  :(NSString *            )methodString
onInterfaceNamed    :(NSString *            )interfaceName
withReturnVerb      :(void(^)(NSString *)   )verb
{
    [self invokeMethodString:methodString onInterfaceNamed:interfaceName withReturnVerb:verb withFlags:0];
}

- (void)
invokeMethodString  :(NSString *            )methodString
onInterfaceNamed    :(NSString *            )interfaceName
withReturnVerb      :(void(^)(NSString *)   )verb
withFlags           :(uint8_t               )flags
{
    DBGParameterAssert(interfaceName != nil);

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        AJNMessageArgument  *argument = [AJNMessageArgument new]; [argument setValue:@"s", methodString.UTF8String];

        AJNMessage          *reply;

        QStatus status
        =
        [self
         callMethodWithName :@(kLNBusObjectMethodMemberName)
         onInterfaceWithName:interfaceName
         withArguments      :@[argument]
         methodReply        :&reply
         timeout            :3000
         flags              :flags];

        if (status == ER_OK) {
            if ([reply type] != kAJNMessageTypeError) {
                if (verb) {
                    char *result = NULL; [reply.arguments.lastObject value:@"s", &result];

                    verb(result? @(result): nil);
                }
            } else {
                DBGWarning(@"Caught remote method call error <name:%@ description:%@>",
                           [reply errorName], [reply errorDescription]);
            }
        } else {
            DBGWarning(@"Failed to call remote method with parameter '%@' on interface named '%@' (%s)",
                       methodString, interfaceName, QCC_StatusText(status));
        }
    });
}

@end
