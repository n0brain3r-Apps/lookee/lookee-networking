//
//  ATLinkedObject.m
//  AllJoynTest
//
//  Created by Ryan on 5/21/14.
//  Copyright (c) 2014 lookee. All rights reserved.
//


#import "LNNode_Private.h"
#import "LNBusObject_Private.h"

#import "AllJoynFramework/AJNInterfaceDescription.h"

#import "LNBusManager.h"

#import "LNBusObject.h"

#import <Utilities/LMacros.h>
#import <Utilities/LQueuingProxy.h>
#import <Utilities/LCallbackProxy.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/NSThread+Block.h>
#import <Utilities/NSString+Utilities.h>
#import <Utilities/NSInvocation+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@interface LNNodeCallback : NSObject

@property (nonatomic, readonly  , strong) void      (^verb)(NSArray *);

@property (nonatomic, readonly  , weak  ) NSThread  *thread;


+ (instancetype)callbackWithVerb:(void(^)(NSArray *))verb;


- (void)executeWithArguments:(NSArray *)arguments;

@end

@interface LNNodeObject : LNBusObject <LNBusObjectDelegate>

@property (nonatomic, readonly, weak) LNNode *node;


- (id)initWithNode:(LNNode *)node atPath:(NSString *)path;


- (void)sendSignal:(NSString *)signal toPeerNamed:(NSString *)peerName inSessionWithID:(AJNSessionId)sessionID;

- (void)
sendSignal      :(NSString *    )signal
toPeerNamed     :(NSString *    )peerName
inSessionWithID :(AJNSessionId  )sessionID
withFlags       :(AJNMessageFlag)flags;

@end

@interface LNNodeLink : NSObject

@property (nonatomic, readonly  , strong) AJNProxyBusObject *handle;

@property (nonatomic, readonly  , strong) NSString          *path;

@property (nonatomic, readonly  , assign) BOOL              isValid;

@property (nonatomic, readonly  , strong) NSString          *peerName;
@property (nonatomic, readonly  , strong) NSString          *sessionName;

@property (nonatomic, readonly  , weak  ) LNNode            *node;


+ (id)linkForNode:(LNNode *)node withProxy:(AJNProxyBusObject *)proxy;

- (NSDictionary *)encodedValuesForProperties;

- (NSString *)encodedValueOfProperty:(NSString *)property;
- (void)setEncodedValue:(NSString *)value ofProperty:(NSString *)property;

- (void)invokeMethodString:(NSString *)parameter withReturnVerb:(void(^)(NSString *))verb;
- (void)invokeMethodString:(NSString *)parameter withReturnVerb:(void(^)(NSString *))verb withFlags:(uint8_t)flags;

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LNNode
#pragma mark -
//*********************************************************************************************************************//




@implementation LNNode

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@public
    NSString        *_ID;
    NSString        *_typeID;
    NSString        *_path;
    LNNode          *_parent;
    void            (^_propertyWillChangeVerb)(NSString *);
    void            (^_propertyDidChangeVerb)(NSString *);

@protected
    LNBusManager *_manager;

@private
    LNNodeObject        *_handle;
    NSPointerArray      *_children;
    NSMutableDictionary *_links;
    NSMutableDictionary *_callbacks;

    id _propertyWillChangeVerbLock;
    id _propertyDidChangeVerbLock;

    id _particle;
}

#pragma mark - Static Objects
//*********************************************************************************************************************//

static NSString *const  kLNNodeInterfacePrefix                  = @"org.lookee.node.";

static NSString *const  kLNNodeMessageNameKey                   = @"_name";
static NSString *const  kLNNodeMessagePeerNameKey               = @"_peer";

static NSString *const  kLNNodeFetchChildrenMessage             = @"fetchChildren";
static NSString *const  kLNNodeFetchChildrenResponseMessage     = @"fetchChildrenResponse";
static NSString *const  kLNNodeFetchChildrenResponseEndMessage  = @"fetchChildrenResponseEnd";

static inline id LNNodeGetPathInfo(NSString *path) {
    return ({
        id components = @split(path.lastPathComponent, @"_");

        id pathInfo;
        if ([components count] == 2){
            pathInfo = @{@"ID"      : components[1],
                         @"typeID"  : components[0]};
        }

        pathInfo;
    });
}

static inline id LNNodeLinkKey(NSString *peerName, NSString *sessionName) {
    return [NSString stringWithFormat:@"%@#%@", peerName, sessionName];
}

static inline id LNNodeLinkKeyComponents(NSString *key) {
    return [key componentsSeparatedByString:@"#"];
}


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (NSMapTable *)instances
{
    static NSMapTable       *instances  = nil;
    static dispatch_once_t  predicate   = 0;

    dispatch_once(&predicate, ^{
        instances = [NSMapTable strongToWeakObjectsMapTable];
    });

    return instances;
}

+ (NSString *)interfaceName
{
    static NSMutableDictionary  *classes    = nil;
    static dispatch_once_t      predicate   = 0;

    dispatch_once(&predicate, ^{
        classes = [NSMutableDictionary new];
    });

    NSString *interfaceName = nil;

    @synchronized(self) {
        NSString *typeID = $classname;

        interfaceName = classes[typeID];

        if (!interfaceName) {
            classes[typeID]
            =
            interfaceName = @join(kLNNodeInterfacePrefix, typeID);
        }
    }

    return interfaceName;
}

+ (LNNode *)findNodeWithPath:(NSString *)path
{
    return [[self instances] objectForKey:path];
}

+ (id)nodeWithPath:(NSString *)path
{
    id instances    = [LNNode instances];
    id pathInfo     = LNNodeGetPathInfo(path);

    if (!pathInfo) {
        DBGError(@"Invalid node path %@", path);
        return nil;
    }

    Class type = NSClassFromString(pathInfo [@"typeID"]);

    if (![type isSubclassOfClass:[LNNode class]]) {
        DBGError(@"Invalid type %@ in %@", type, path);
        return nil;
    }

    id parent;

    @synchronized(instances) {
        parent = [instances objectForKey:path.stringByDeletingLastPathComponent];
    }

    return [[type alloc]
            initWithManager :[parent manager]
            parent          :(parent)
            typeID          :pathInfo [@"typeID"]
            ID              :pathInfo [@"ID"]
            encodedID       :YES];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)initWithManager:(LNBusManager *)manager ID:(NSString *)ID
{
    if ((self = [self initWithManager:manager parent:nil ID:ID])) {

    }

    return self;
}

- (id)initWithParent:(LNNode *)parent ID:(NSString *)ID
{
    if ((self = [self initWithManager:parent.manager parent:parent ID:ID])) {

    }

    return self;
}

- (id)initWithManager:(LNBusManager *)manager parent:(LNNode *)parent ID:(NSString *)ID
{
    if ((self = [self initWithManager:manager parent:parent typeID:$classname ID:ID])) {

    }

    return self;
}

- (id)
initWithManager :(LNBusManager *)manager
parent          :(LNNode *      )parent
typeID          :(NSString *    )typeID
ID              :(NSString *    )ID
{
    if ((self = [self initWithManager:manager parent:parent typeID:typeID ID:ID encodedID:NO])) {

    }

    return self;
}

- (id)
initWithManager :(LNBusManager *)manager
parent          :(LNNode *      )parent
typeID          :(NSString *    )typeID
ID              :(NSString *    )ID
encodedID       :(BOOL          )encodedID
{
    DBGParameterAssert((parent == nil || parent.path != nil) && (typeID != nil) && (ID != nil));

    id instances    = [LNNode instances];

    id path         = [NSString stringWithFormat:@"%@/%@_%@",
                       parent? [parent path]: [NSString new],
                       typeID,
                       encodedID? ID: [ID readableRepresentation]];

    id node;

    @synchronized(instances) {
        node = [instances objectForKey:path];
    }

    if (node) {
        return node;
    }

    self = [super init];

    if (self) {
        _propertyWillChangeVerbLock = [NSObject new];
        _propertyDidChangeVerbLock  = [NSObject new];

        _particle   = [LCallbackProxy proxyWithType:$this];

        _parent     = parent;
        _children   = [NSPointerArray weakObjectsPointerArray];

        _ID         = [ID copy];
        _typeID     = [typeID copy];
        _path       = path;

        _manager    = manager;

        _handle     = [[LNNodeObject alloc] initWithNode:self atPath:path];

        _links      = [LQueuingProxy proxyWithObject:[NSMutableDictionary new]];
        _callbacks  = [LQueuingProxy proxyWithObject:[NSMutableDictionary new]];

        [self.manager registerBusObject:_handle];

        if (_parent) {
            @synchronized(_parent->_children) {
                [_parent->_children addPointer:Voidify(self)];
            }

            if ([[_parent class] shouldManageChildren]) {
                [self linkToNode:_parent];
            }
        }

        [self startMonitoringProperties];
        [self startMonitoringMessages];
    }

    return self;
}

- (void)dealloc
{
    DBGMessage(@"deallocated %@", self);

    [self.manager unregisterBusObject:_handle], (_handle = nil);
    [_links removeAllObjects];
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:$this]) {
        return [self.path isEqualToString:[(LNNode *)(object) path]];
    }

    return NO;
}

- (NSComparisonResult)compare:(id)object
{
    assert([object isKindOfClass:[LNNode class]]);

    LNNode *node = object;
    {
        return [[self path] compare:[node path]];
    }
}

- (NSUInteger)hash
{
    return [self.path hash];
}

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, path: %@>",
            $classname,
            self,
            self.path];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (BOOL)isLinkedToPeerNamed:(NSString *)peerName inSessionNamed:(NSString *)sessionName
{
    return [self isLinkedToPeerWithKey:LNNodeLinkKey(peerName, sessionName)];
}

- (void)linkToPeerNamed:(NSString *)peerName inSessionNamed:(NSString *)sessionName
{
    DBGParameterAssert((peerName != nil) && (sessionName != nil));

    if (![self.manager.peerName isEqualToString:peerName]) {
        [self linkToPeerWithKey:LNNodeLinkKey(peerName, sessionName)];
    }
}

- (void)linkToNode:(LNNode *)node
{
    DBGParameterAssert(node != nil);

    for (LNNodeLink *link in [node->_links allValues]) {
        [self linkToPeerNamed:link.peerName inSessionNamed:link.sessionName];
    }
}

- (void)unlinkFromPeerNamed:(NSString *)peerName inSessionNamed:(NSString *)sessionName
{
    DBGParameterAssert((peerName != nil) && (sessionName != nil));

    [self unlinkFromPeerWithKey:LNNodeLinkKey(peerName, sessionName)];
}

- (void)unlink
{
    [_links removeAllObjects];

    if ([$this shouldManageChildren]) {
        for (LNNode *child = [self firstChild]; child != nil; child = [self nextChild]) {
            [child unlink];
        }
    }
}

- (LSendVerbType)sendMessage:(NSString *)name
{
    DBGParameterAssert(name != nil);

    @weakify();
    return (^(id arguments) {
        @strongify();

        id message  = [arguments mutableCopy];

        id peerName;

        if ([arguments isKindOfClass:[NSArray class]]) {
            [message insertObject:name atIndex:0];
        } else {
            [message setObject:name forKey:kLNNodeMessageNameKey];

            peerName = message[kLNNodeMessagePeerNameKey];
            [message removeObjectForKey:kLNNodeMessagePeerNameKey];
        }

        id signal   = [NSString stringFromObjectRepresentation:message];

        for (LNNodeLink *link in [self->_links allValues]) {
            if ([link isValid]) {
                if (!(peerName) || [peerName isEqualToString:link.peerName]) {
                    [self->_handle sendSignal:signal toPeerNamed:link.peerName inSessionWithID:link.handle.sessionId];
                    
                    if (peerName) {
                        break;
                    }
                }
            }
        }
    });
}

- (LReceiveVerbType)interceptMessage:(NSString *)name
{
    DBGParameterAssert(name != nil);

    @weakify();
    return ^(void(^verb)(NSArray *)) {
        @strongify();

        self->_callbacks[name] = [LNNodeCallback callbackWithVerb:verb];
    };
}

- (void)ignoreMessage:(NSString *)name
{
    DBGParameterAssert(name != nil);

    [_callbacks removeObjectForKey:name];
}

- (void)fetchPropertyValue:(NSString *)propertyName fromPeerNamed:(NSString *)peerName
{
    DBGParameterAssert(propertyName != nil);

    for (LNNodeLink *link in [_links allValues]) {
        if ([link isValid] && [link.peerName isEqualToString:peerName]) {
            [self setEncodedValue:[link encodedValueOfProperty:propertyName] forKey:propertyName];
            break;
        }
    }
}

- (void)fetchPropertyValuesFromPeerNamed:(NSString *)peerName
{
    for (LNNodeLink *link in [_links allValues]) {
        if ([link isValid] && [link.peerName isEqualToString:peerName]) {
            NSDictionary *encodedValues = [link encodedValuesForProperties];

            for (NSString *key in [encodedValues allKeys]) {
                [self setEncodedValue:encodedValues[key] forKey:key];
            }
            break;
        }
    }
}

- (id)firstChild
{
    id store    = [$thisthread threadDictionary];
    id key      = @join(self.path, @"._iterator");

    store[key]  = @-1;

    return [self nextChild];
}

- (id)nextChild
{
    id store    = [$thisthread threadDictionary];
    id key      = @join(self.path, @"._iterator");
    id tag      = store[key];
    
    if (!tag) {
        DBGError(@"Iterator not initialized");
        return nil;
    }

    NSInteger index = [tag integerValue]+1;

    @synchronized(_children) {
        while (index < [_children count]) {
            id child = [_children pointerAtIndex:index];

            if (!child) {
                [_children removePointerAtIndex:index];
                continue;
            }

            store[key] = @(index);

            return child;
        }

        return nil;
    }
}

- (void)foreachChild:(void(^)(id node))verb
{
    DBGParameterAssert(verb != nil);

    __weak __block id innerIterator;

    id iterator = ^(NSString *path, id object) {
        if (object) {
            verb(object), [[self remote:innerIterator] nextChild];
        }
    };

    (innerIterator = iterator), [[self remote:iterator] firstChild];
}

- (void)fetchChildren:(void(^)(NSArray *))block
{
    DBGParameterAssert(block != nil);

    NSMutableArray *children = [NSMutableArray new];

    [self interceptMessage:kLNNodeFetchChildrenResponseMessage] (^(NSDictionary *arguments) {
        LNNode *node = [LNNode nodeWithPath:arguments[@"path"]];

        [node mergePropertiesFromJSONString:arguments[@"metadata"]];

        [children addObject:node];
    });

    @weakify();
    [self interceptMessage:kLNNodeFetchChildrenResponseEndMessage] (^(NSDictionary *arguments) {
        @strongify();

        [self ignoreMessage:kLNNodeFetchChildrenResponseMessage];
        [self ignoreMessage:kLNNodeFetchChildrenResponseEndMessage];

        block(({
            NSMutableArray *children = [NSMutableArray new];

            for (LNNode *child = [self firstChild]; child != nil; child = [self nextChild]) {
                [children addObject:child];
            }

            (NSArray *)( children );
        }));

        (void)( children );
    });

    [self sendMessage:kLNNodeFetchChildrenMessage](@{ });
}


#pragma mark - Protected Methods
//*********************************************************************************************************************//

- (void)willChangeEncodedValueForProperty:(NSString *)property
{
    @synchronized(_propertyWillChangeVerbLock) {
        if (_propertyWillChangeVerb) {
            _propertyWillChangeVerb(property);
        }
    }
}

- (void)didChangeEncodedValueForProperty:(NSString *)property
{
    @synchronized(_propertyDidChangeVerbLock) {
        if (_propertyDidChangeVerb) {
            _propertyDidChangeVerb(property);
        }
    }
}

- (NSString *)sessionNameForLink:(LNNodeLink *)link
{
    return [self.manager sessionNameForRemoteObject:link.handle];
}

- (NSString *)interfaceForEncodableProperty:(NSString *)propertyName
{
    for (AJNInterfaceDescription *interface in [_handle interfaces].allValues) {
        for (AJNInterfaceProperty *property in [interface properties]) {
            if ([property.name isEqualToString:propertyName]) {
                return [interface name];
            }
        }
    }

    return nil;
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)startMonitoringProperties
{
    NSMutableArray *types = [@[$this] mutableCopy];
    [types addObjectsFromArray:[[$this superclasses] array]];

    NSMutableSet *propertyNames = [NSMutableSet new];

    for (Class type in types) {
        if ([type isSubclassOfClass:[LNNode class]]) {
            for (NSString *propertyName in [type propertyNames]) {
                [propertyNames addObject:propertyName];
            }
        }
    }

    if ([propertyNames count]) {
        @weakify();
        [self observerForKeyPaths:[propertyNames allObjects]] (^(LKVONotification *notification) {
            @strongify();

            NSString    *propertyName   = [notification keyPath];

            id          previousValue   = [notification oldValue];
            id          currentValue    = [notification newValue];

            if (![previousValue isEqual:currentValue]) {
                for (LNNodeLink *link in [self->_links allValues]) {
                    if ([link isValid]) {
                        [link setEncodedValue:[self encodedValueForKey:propertyName] ofProperty:propertyName];
                    }
                }
            }
        });
    }
}

- (void)startMonitoringMessages
{
    @weakify();
    [self interceptMessage:kLNNodeFetchChildrenMessage] (^(NSDictionary *arguments) {
        @strongify();

        for (LNNode *child = [self firstChild]; child != nil; child = [self nextChild]) {
            id response = [arguments mutableCopy];
            {
                response[@"path"    ] = [child path];
                response[@"metadata"] = [child JSONStringFromProperties];
            }

            [self sendMessage:kLNNodeFetchChildrenResponseMessage]( response );
        }

        [self sendMessage:kLNNodeFetchChildrenResponseEndMessage]( arguments );
    });
}

- (LNNodeLink *)createLinkForNodeAtPeerNamed:(NSString *)peerName inSessionNamed:(NSString *)sessionName
{
    LNNodeLink *link
    =
    [LNNodeLink
     linkForNode    :self
     withProxy      :
     [[self manager]
      remoteObjectWithType  :[AJNProxyBusObject class]
      withPath              :[self path]
      atPeerNamed           :peerName
      inSessionNamed        :sessionName]
     ];

    for (AJNInterfaceDescription *interface in [_handle interfaces].allValues) {
        [link.handle addInterfaceFromDescription:interface];
    }

    return link;
}

- (BOOL)isLinkedToPeerWithKey:(NSString *)key
{
    return !![_links objectForKey:key];
}

- (void)linkToPeerWithKey:(NSString *)key
{
    LNNodeLink *link = [_links objectForKey:key];

    if (!link || ![link isValid]) {
        NSArray *components = LNNodeLinkKeyComponents(key);

        if ((link = [self createLinkForNodeAtPeerNamed:components[0] inSessionNamed:components[1]])) {
            [_links setObject:link forKey:key];
        }
    }

    if ([$this shouldManageChildren]) {
        for (LNNode *child = [self firstChild]; child != nil; child = [self nextChild]) {
            [child linkToPeerWithKey:key];
        }
    }
}

- (void)unlinkFromPeerWithKey:(NSString *)key
{
    DBGParameterAssert((key != nil));

    [_links removeObjectForKey:key];

    if ([$this shouldManageChildren]) {
        for (LNNode *child = [self firstChild]; child != nil; child = [self nextChild]) {
            [child unlinkFromPeerWithKey:key];
        }
    }
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Category Methods
#pragma mark -
//*********************************************************************************************************************//




@implementation LNNode (Properties)

- (LNBusManager *)manager
{
    return _manager;
}

- (NSString *)ID
{
    return _ID;
}

- (NSString *)typeID
{
    return _typeID;
}

- (NSString *)path
{
    return _path;
}

- (LNNode *)parent
{
    return _parent;
}

- (void)setPropertyWillChangeVerb:(void(^)(NSString *))propertyWillChangeVerb
{
    @synchronized(_propertyWillChangeVerbLock) {
        _propertyWillChangeVerb = propertyWillChangeVerb;
    }
}

- (void)setPropertyDidChangeVerb:(void(^)(NSString *))propertyDidChangeVerb
{
    @synchronized(_propertyDidChangeVerbLock) {
        _propertyDidChangeVerb = propertyDidChangeVerb;
    }
}

- (id)remote
{
    return [self remote:nil];
}

- (id)remote:(LMethodReturnVerbType)verb
{
    id links = [_links allValues];

    [_particle $setCallback:

     ^(NSInvocation *invocation) {
         id methodString = [invocation toJSONString];

         for (LNNodeLink *link in links) {
             if ([link isValid]) {
                 [link invokeMethodString:methodString withReturnVerb:

                  ^(NSString *resultString) {
                      if (verb) {
                          verb([link peerName], resultString? [NSObject instanceFromJSONString:resultString]: nil);
                      }
                  }

                  ];
             }
         }
     }

     ];

    return( _particle );
}

@end

@implementation LNNode (Virtual)

+ (BOOL)shouldManageChildren
{
    return YES;
}

- (NSString *)encodedValueForKey:(NSString *)key
{
    return [[self valueForKey:key] toJSONString];
}

- (void)setEncodedValue:(NSString *)value forKey:(NSString *)key
{
    [self setValue:[NSObject instanceFromJSONString:value] forKey:key];
}

- (void)receiveSignal:(NSString *)signal fromPeerNamed:(NSString *)peerName
{
    id message = [signal.objectRepresentation mutableCopy];

    id name;

    if ([message isKindOfClass:[NSArray class]]) {
        name = message[0];

        [message insertObject:peerName atIndex:0];
    } else {
        name = message[kLNNodeMessageNameKey];

        [message setObject:peerName forKey:kLNNodeMessagePeerNameKey];
    }

    [_callbacks[name] executeWithArguments:message];
}

- (NSString *)invokeMethodString:(NSString *)methodString
{
    id invocation = [NSInvocation instanceFromJSONString:methodString];

    [invocation invokeWithTarget:self];

    id result = [invocation simplifiedReturnValue];

    if (result) {
        return [result toJSONString];
    }
    
    return nil;
}

@end

@implementation LNNode (Serialization)

- (id)initWithSimplifiedRepresentation:(id)representation
{
    id path = representation[0], properties = representation[1];
    {
        id instances    = [LNNode instances];
        id pathInfo     = LNNodeGetPathInfo(path);

        if (!pathInfo) {
            DBGError(@"Invalid node path %@", path);
            return nil;
        }

        id parent;

        @synchronized(instances) {
            parent = [instances objectForKey:[path stringByDeletingLastPathComponent]];
        }

        self
        =
        [self initWithManager   :[parent manager]
              parent            :(parent)
              typeID            :pathInfo [@"typeID"]
              ID                :pathInfo [@"ID"]
              encodedID         :YES];

        if (self) {
            [self mergePropertiesFromDictionary:properties];
        }

        return self;
    }
}

- (id)simplifiedRepresentation
{
    return @[[self path], [self dictionaryFromProperties]];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LNNodeCallback

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)callbackWithVerb:(void(^)(NSArray *))verb
{
    return [[self alloc] initWithVerb:verb];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)initWithVerb:(void(^)(NSArray *))verb
{
    NSParameterAssert(verb != nil);

    if ((self = [super init])) {
        _thread = $thisthread;
        _verb   = [verb copy];
    }

    return self;
}


#pragma mark - Public Method
//*********************************************************************************************************************//

- (void)executeWithArguments:(NSArray *)arguments
{
    typeof(self.verb) verb = [self verb];

    [self.thread performBlock:^{
        verb(arguments);
    }];
}

@end


@implementation LNNodeObject

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)initWithNode:(LNNode *)node atPath:(NSString *)path
{ // TODO: Remove path parameter
    if ((self = [super initWithPath:path])) {
        _node = node;
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, node: %@>",
            $classname,
            self,
            self.node];
}

- (NSString *)encodedValueForKey:(NSString *)key
{
    if ([self.node interfaceForEncodableProperty:key]) {
        NSString *value = [self.node encodedValueForKey:key];

        if (value) {
            return value;
        } else {
            DBGWarning(@"Invalid encoded value '%@' for %@[%@]", value, self.node.class, key);
        }
    } else {
        DBGError(@"Unable to find key '%@' in %@ interfaces", key, self.node.class);
    }

    return [NSString new];
}

- (void)setEncodedValue:(NSString *)value forKey:(NSString *)key
{
    if ([value length]) {
        if ([self.node interfaceForEncodableProperty:key]) {
            [self.node willChangeEncodedValueForProperty:key];
            [self.node setEncodedValue:value forKey:key];
            [self.node didChangeEncodedValueForProperty:key];
        } else {
            DBGError(@"Unable to find key '%@' in %@ interfaces", key, self.node.class);
        }
    } else {
        DBGError(@"Invalid encoded value '%@' for %@[%@]", value, self.node.class, key);
    }
}

- (void)receiveSignal:(NSString *)signal fromPeerNamed:(NSString *)peerName
{
    if ([signal length]) {
        if ([peerName length]) {
            [self.node receiveSignal:signal fromPeerNamed:peerName];
        } else {
            DBGWarning(@"Unidentified sender of encoded signal '%@' for %@", signal, self.node.class);
        }
    } else {
        DBGWarning(@"Invalid encoded signal '%@' for %@", signal, self.node.class);
    }
}

- (NSString *)invokeMethodString:(NSString *)methodString
{
    if ([methodString length]) {
        return [self.node invokeMethodString:methodString];
    } else {
        DBGWarning(@"Invalid method string '%@' for %@", methodString, self.node.class);
    }

    return nil;
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)sendSignal:(NSString *)signal toPeerNamed:(NSString *)peerName inSessionWithID:(AJNSessionId)sessionID
{
    [self sendSignal:signal toPeerNamed:peerName inSessionWithID:sessionID withFlags:0];
}

- (void)
sendSignal      :(NSString *    )signal
toPeerNamed     :(NSString *    )peerName
inSessionWithID :(AJNSessionId  )sessionID
withFlags       :(AJNMessageFlag)flags
{
    if ([signal length]) {
        NSString *interfaceName = [self.node.class interfaceName];

        if (interfaceName) {
            [self
             sendSignal         :signal
             toPeerNamed        :peerName
             inSessionWithID    :sessionID
             onInterfaceNamed   :interfaceName
             withFlags          :flags];
        } else {
            DBGError(@"Invalid interface name '%@' for %@", interfaceName, self.node.class);
        }
    } else {
        DBGError(@"Invalid encoded signal '%@' for %@", signal, self.node.class);
    }
}


#pragma mark - LNBusObjectDelegate
//*********************************************************************************************************************//

- (void)registerInterfacesWithBus:(AJNBusAttachment *)busAttachment
{
    NSMutableArray *interfaceNames = [NSMutableArray new];

    NSMutableArray *types = [@[self.node.class] mutableCopy];
    [types addObjectsFromArray:[[self.node.class superclasses] array]];

    for (Class type in types) {
        if ([type respondsToSelector:@selector(interfaceName)]) {
            [interfaceNames addObject:[type interfaceName]];
        }
    }

    [self initializeInterfacesWithNames:interfaceNames forBusAttachment:busAttachment];
}

@end


@implementation LNNodeLink

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (id)linkForNode:(LNNode *)node withProxy:(AJNProxyBusObject *)proxy
{
    return [[self alloc] initWithNode:node withProxy:proxy];
}


#pragma mark - Constructors
//*********************************************************************************************************************//

- (id)initWithNode:(LNNode *)node withProxy:(AJNProxyBusObject *)proxy
{
    if ((self = [super init])) {
        _node           = node;
        _handle         = proxy;
        _sessionName    = [self.node sessionNameForLink:self];
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, peerName: %@, sessionName: %@, isValid: %@, node: %@>",
            $classname,
            self,
            self.peerName,
            self.sessionName,
            self.isValid?
            @"YES":@"NO",
            self.node];
}


#pragma mark - Properties
//*********************************************************************************************************************//

@synthesize node = _node;

@dynamic isValid;

- (BOOL)isValid
{
    return [self.handle isValid];
}

@dynamic path;

- (NSString *)path
{
    return [self.handle path];
}

@synthesize sessionName = _sessionName;

@dynamic peerName;

- (NSString *)peerName
{
    return [self.handle serviceName];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (NSDictionary *)encodedValuesForProperties
{
    NSString *interfaceName = [self.node.class interfaceName];

    if (interfaceName) {
        return [self.handle encodedValuesForPropertiesForInterfaceNamed:interfaceName];
    } else {
        DBGError(@"Invalid interface name '%@' for %@", interfaceName, self.node.class);
    }

    return nil;
}

- (NSString *)encodedValueOfProperty:(NSString *)property
{
    NSString *interfaceName = [self.node interfaceForEncodableProperty:property];

    if (interfaceName) {
        NSString *value = [self.handle encodedValueOfProperty:property forInterfaceNamed:interfaceName];

        if ([value length]) {
            return value;
        } else {
            DBGWarning(@"Invalid encoded value '%@' for %@.%@", value, self.node.class, property);
        }
    } else {
        DBGError(@"Unable to find property '%@' in %@ interfaces", property, self.node.class);
    }

    return nil;
}

- (void)setEncodedValue:(NSString *)value ofProperty:(NSString *)property
{
    if (value) {
        NSString *interfaceName = [self.node interfaceForEncodableProperty:property];

        if (interfaceName) {
            [self.handle setEncodedValue:value ofProperty:property forInterfaceNamed:interfaceName];
        } else {
            DBGError(@"Unable to find property '%@' in %@ interfaces", property, self.node.class);
        }
    } else {
        DBGError(@"Invalid encoded value '%@' for %@.%@", value, self.node.class, property);
    }
}

- (void)invokeMethodString:(NSString *)parameter withReturnVerb:(void(^)(NSString *))verb
{
    [self invokeMethodString:parameter withReturnVerb:verb withFlags:0];
}

- (void)invokeMethodString:(NSString *)methodString withReturnVerb:(void(^)(NSString *))verb withFlags:(uint8_t)flags
{
    if ([methodString length]) {
        NSString *interfaceName = [self.node.class interfaceName];

        if (interfaceName) {
            [self.handle
             invokeMethodString :methodString
             onInterfaceNamed   :interfaceName
             withReturnVerb     :verb
             withFlags          :flags];
        } else {
            DBGError(@"Invalid interface name '%@' for %@", interfaceName, self.node.class);
        }
    } else {
        DBGError(@"Invalid encoded parameter '%@' for method call through %@", methodString, self.node.class);
    }
}

@end
