//
//  LNBusObject.h
//  AllJoynTest
//
//  Created by Ryan on 5/20/14.
//  Copyright (c) 2014 lookee. All rights reserved.
//


#import "AllJoynFramework/AJNBusObject.h"
#import "AllJoynFramework/AJNProxyBusObject.h"


@interface LNBusObject : AJNBusObject

- (void)initializeInterfacesWithNames:(NSArray *)interfaceNames forBusAttachment:(AJNBusAttachment *)busAttachment;

- (void)
sendSignal      :(NSString *    )signal
toPeerNamed     :(NSString *    )peerName
inSessionWithID :(AJNSessionId  )sessionID
onInterfaceNamed:(NSString *    )interfaceName;

- (void)
sendSignal      :(NSString *    )signal
toPeerNamed     :(NSString *    )peerName
inSessionWithID :(AJNSessionId  )sessionID
onInterfaceNamed:(NSString *    )interfaceName
withFlags       :(AJNMessageFlag)flags;

@end

@interface LNBusObject (Properties)

- (NSDictionary *)interfaces;

@end


@interface AJNProxyBusObject (LNBusObjectProxy)

- (NSDictionary *)encodedValuesForPropertiesForInterfaceNamed:(NSString *)interfaceName;

- (NSString *)encodedValueOfProperty:(NSString *)property forInterfaceNamed:(NSString *)interfaceName;

- (void)
setEncodedValue     :(NSString *    )value
ofProperty          :(NSString *    )property
forInterfaceNamed   :(NSString *    )interfaceName;

- (void)
invokeMethodString  :(NSString *            )methodString
onInterfaceNamed    :(NSString *            )interfaceName
withReturnVerb      :(void(^)(NSString *)   )verb;

- (void)
invokeMethodString  :(NSString *            )methodString
onInterfaceNamed    :(NSString *            )interfaceName
withReturnVerb      :(void(^)(NSString *)   )verb
withFlags           :(uint8_t               )flags;

@end