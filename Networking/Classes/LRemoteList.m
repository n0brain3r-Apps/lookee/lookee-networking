//
//  LNNodeList.m
//  Writeability
//
//  Created by Ryan on 6/19/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LRemoteList_Private.h"

#import <Utilities/LMacros.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Node Description
#pragma mark -
//*********************************************************************************************************************//




@interface LNNodeList : LNNode

@end

@implementation LNNodeList

@end

@interface LNNodeSubList : LNNodeList

@end

@implementation LNNodeSubList

+ (BOOL)shouldManageChildren
{
    return NO;
}

@end

@interface LNNodeListElement : LNNode

@property (nonatomic, copy) NSDictionary *attributes;

@end

@implementation LNNodeListElement

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//



// TODO: Figure out a way to avoid using LNNodeListForwarder for LNNode objects
@interface LNNodeListForwarder : NSProxy
{
@package
    id object;

    LNNodeListElement *element;
}

+ (instancetype)forwarderWithLinkedElement:(LNNodeListElement *)element;
+ (instancetype)forwarderWithAttributes:(NSDictionary *)attributes;
+ (instancetype)forwarderWithList:(LNNodeList *)list object:(id<LRemoteObject>)object;

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LRemoteList
#pragma mark -
//*********************************************************************************************************************//




@interface LRemoteList () <LRemoteObject>
 
@property (nonatomic, readonly, strong) NSMutableOrderedSet *nodes;

@end

@implementation LRemoteList $remote(LNNodeList)

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)listWithNode:(LNNode *)node
{
    return [[self alloc] initWithNode:node];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)initWithNode:(LNNode *)node ID:(NSString *)ID
{
    if ((self = [super initWithNode:node ID:ID])) {
        _nodes = [NSMutableOrderedSet new];

        [self initialize];
    }

    return self;
}

- (void)initialize
{
    NSOperationQueue *queue = $thisqueue;

    @weakify();

    [self.node interceptMessage:@"insertObject:atIndex:"] (^(NSArray *arguments) {
        @strongify();

        [queue addOperationWithBlock:^{
            [self insertObjectWithAttributes:arguments[2] atIndex:[arguments[3] integerValue]];
        }];
    });

    [self.node interceptMessage:@"removeObjectAtIndex:"] (^(NSArray *arguments) {
        @strongify();

        [queue addOperationWithBlock:^{
            [self removeObjectWithAttributes:arguments[2] atIndex:[arguments[3] integerValue]];
        }];
    });

    [self.node fetchChildren:^(NSArray *children) {
        for (id element in children) {
            if ([element isKindOfClass:[LNNodeListElement class]]) {
                [self addNodesObject:[LNNodeListForwarder forwarderWithLinkedElement:element]];
            }
        }
    }];
}

- (void)dealloc
{
    [self.nodes removeAllObjects];
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)setDidInsertAtIndexVerb:(void(^)(NSUInteger))didInsertAtIndexVerb
{
    _didInsertAtIndexVerb = didInsertAtIndexVerb;
}


#pragma mark - LRemoteObject
//*********************************************************************************************************************//

- (id)initWithStringRepresentation:(NSString *)representation
{
    LNNode *parent   = [LNNode findNodeWithPath:representation.stringByDeletingLastPathComponent];

    LNNode *node     = [LNNodeList nodeWithPath:representation];

    [node linkToNode:parent];

    if ((self = [self initWithNode:node ID:node.ID])) {

    }

    return self;
}

- (NSString *)stringRepresentation
{
    return [self.node path];
}

- (Class)resolveType
{
    if (![self isKindOfClass:[LRemoteArray class]]) {
        return [self class];
    }

    return [LRemoteList class];
}

- (BOOL)isValid
{
    return YES;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, objects:(\n%@\n)>",
            $classname,
            self,
            [self.nodes.array componentsJoinedByString:@",\n"]];
}


#pragma mark - Array Accessors
//*********************************************************************************************************************//

- (NSUInteger)countOfNodes
{
    return [self.nodes count];
}

- (LNNodeListForwarder *)objectInNodesAtIndex:(NSUInteger)index
{
    return [self nodes][ index ];
}

- (void)addNodesObject:(LNNodeListForwarder *)object
{
    [self insertObject:object inNodesAtIndex:self.count];
}

- (void)insertObject:(LNNodeListForwarder *)object inNodesAtIndex:(NSUInteger)index
{
    [self.nodes insertObject:object atIndex:index];

    if ([self didInsertAtIndexVerb]) {
        [self didInsertAtIndexVerb](index);
    }
}

- (void)removeObjectFromNodesAtIndex:(NSUInteger)index
{
    if ([self willRemoveAtIndexVerb]) {
        [self willRemoveAtIndexVerb](index);
    }

    [self.nodes removeObjectAtIndex:index];
}


#pragma mark - NSFastEnumeration
//*********************************************************************************************************************//

- (NSUInteger)
countByEnumeratingWithState :(NSFastEnumerationState *  )state
objects                     :(__unsafe_unretained id [] )buffer
count                       :(NSUInteger                )len
{
    return [self.nodes countByEnumeratingWithState:state objects:buffer count:len];
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic count;

- (NSUInteger)count
{
    return [self countOfNodes];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (id<LRemoteObject>)objectAtIndex:(NSUInteger)index
{
    if (index >= [self count]) {
        DBGError(@"Cannot retrieve object at invalid index %d", index);
        return nil;
    }

    return [self objectInNodesAtIndex:index]->object;
}

- (id<LRemoteObject>)objectAtIndexedSubscript:(NSUInteger)index
{
    return [self objectAtIndex:index];
}

- (id<LRemoteObject>)lastObject
{
    NSUInteger count = [self count];

    if (count) {
        return [self objectAtIndex:count-1];
    }

    return nil;
}

- (NSUInteger)indexOfObject:(id<LRemoteObject>)object
{
    return
    [self.nodes indexOfObjectPassingTest:
     ^(LNNodeListForwarder *forwarder, NSUInteger index, BOOL *stop) {
         return (*stop = [forwarder->object isEqual:object]);
     }];
}

- (NSUInteger)indexOfObjectWithRepresentation:(NSString *)representation
{
    return
    [self.nodes indexOfObjectPassingTest:
     ^(LNNodeListForwarder *forwarder, NSUInteger index, BOOL *stop) {
         return (*stop = [[forwarder->object stringRepresentation] isEqualToString:representation]);
     }];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)insertObjectWithAttributes:(NSDictionary *)attributes atIndex:(NSUInteger)index
{
    if (index > [self count]) {
        DBGError(@"Attempt to insert object at invalid index %d", index);
        return;
    }

    [self insertObject:[LNNodeListForwarder forwarderWithAttributes:attributes] inNodesAtIndex:index];
}

- (void)removeObjectWithAttributes:(NSDictionary *)attributes atIndex:(NSUInteger)index
{
    NSUInteger location = [self indexOfObjectWithRepresentation:attributes[ @"representation" ]];

    if (index != location) {
        DBGWarning(@"Remote and local object indices do not match");
        return;
    }

    [self removeObjectFromNodesAtIndex:location];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LRemoteArray
#pragma mark -
//*********************************************************************************************************************//




@implementation LRemoteArray

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)arrayWithAncestor:(LNRemote *)ancestor
{
    NSString *ID = $UUID;

    LNNode *node;

    if ([ancestor isKindOfClass:[LRemoteArray class]]) {
        node = [[LNNodeSubList alloc] initWithParent:ancestor->_node ID:ID];
    } else {
        node = [[LNNodeList alloc] initWithParent:ancestor->_node ID:ID];
    }

    return [[self alloc] initWithNode:node ID:ID];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (void)initialize
{
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)addObject:(id<LRemoteObject>)object
{
    [self insertObject:object atIndex:self.count];
}

- (void)insertObject:(id<LRemoteObject>)object atIndex:(NSUInteger)index
{
    if (index > [self count]) {
        DBGError(@"Attempt to insert object at invalid index %d", index);
        return;
    }

    [self insertObject:[LNNodeListForwarder forwarderWithList:self.node object:object] inNodesAtIndex:index];

    NSArray *parameters = [self parametersForElementAtIndex:index];

    [self.node sendMessage:@"insertObject:atIndex:"]( parameters );
}

- (void)setObject:(id<LRemoteObject>)object atIndexedSubscript:(NSUInteger)index
{
    [self insertObject:object atIndex:index];
}

- (LRemoteArray *)insertArrayAtLastIndex
{
    return [self insertArrayAtIndex:self.count];
}

- (LRemoteArray *)insertArrayAtIndex:(NSUInteger)index
{
    if (index > [self count]) {
        DBGError(@"Attempt to insert object at invalid index %d", index);
        return nil;
    }

    LRemoteArray *array = [LRemoteArray arrayWithAncestor:self];

    [array->_node linkToNode:self.node];

    [self insertObject:array atIndex:index];

    return array;
}

- (void)removeLastObject
{
    NSUInteger count = [self count];

    if (count) {
        [self removeObjectAtIndex:count-1];
    }
}

- (void)removeObjectAtIndex:(NSUInteger)index
{
    if (index >= [self count]) {
        DBGError(@"Attempt to remove object at invalid index %d", index);
        return;
    }

    NSArray *parameters = [self parametersForElementAtIndex:index];

    [self removeObjectFromNodesAtIndex:index];

    [self.node sendMessage:@"removeObjectAtIndex:"]( parameters );
}

- (void)removeObject:(id<LRemoteObject>)object
{
    NSUInteger index = [self indexOfObject:object];

    if (index != NSNotFound) {
        [self removeObjectAtIndex:index];
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (NSArray *)parametersForElementAtIndex:(NSUInteger)index
{
    LNNodeListForwarder  *forwarder  = [self nodes][ index ];

    NSMutableDictionary *attributes = [[forwarder->element attributes] mutableCopy];

    [attributes addEntriesFromDictionary:@{ @"path": [forwarder->element path] }];

    return @[ attributes, @(index) ];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LNNodeListForwarder

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)forwarderWithLinkedElement:(LNNodeListElement *)element
{
    LNNodeListForwarder *instance = [self alloc];
    {
        id attributes = [element attributes];

        if (attributes) {
            id type             = attributes[ @"type"             ];
            id representation   = attributes[ @"representation"   ];

            instance->element   = element;

            instance->object    = [[NSClassFromString(type) alloc] initWithStringRepresentation:representation];
        }
    }

    return instance;
}

+ (instancetype)forwarderWithList:(LNNodeList *)list object:(id<LRemoteObject>)object
{
    LNNodeListForwarder *instance = [self alloc];
    {
        id type = [[object respondsToSelector:@selector(resolveType)]?
                   [object resolveType] : [object class] description];

        instance->object    = object;

        instance->element   = [[LNNodeListElement alloc] initWithParent:list ID:$UUID];

        id attributes       = [NSMutableDictionary dictionaryWithObject:[type description] forKey:@"type"];

        id representation = [object stringRepresentation];

        if (representation) {
            attributes[ @"representation" ] = representation;
        }

        [instance->element setAttributes:attributes];
    }

    return instance;
}

+ (instancetype)forwarderWithAttributes:(NSDictionary *)attributes
{
    DBGParameterAssert(attributes != nil);

    LNNodeListForwarder *instance = [self alloc];
    {
        id path             = attributes[ @"path"             ];
        id type             = attributes[ @"type"             ];
        id representation   = attributes[ @"representation"   ];

        instance->object    = [[NSClassFromString(type) alloc] initWithStringRepresentation:representation];

        instance->element   = [LNNodeListElement nodeWithPath:path];

        [instance->element setAttributes:attributes];
    }

    return instance;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector
{
    return [object methodSignatureForSelector:selector];
}

- (BOOL)respondsToSelector:(SEL)selector
{
    return [object respondsToSelector:selector];
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
    [invocation invokeWithTarget:object];
}

- (NSString *)description
{
    return [object description];
}

@end
