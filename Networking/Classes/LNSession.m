//
//  LNSession.m
//  Writeability
//
//  Created by Ryan on 5/9/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNSession_Private.h"
#import "LNUser_Private.h"

#import "LNetworking.h"

#import "LNFileThumb.h"

#import "LNBusManager.h"

#import <Utilities/LMacros.h>
#import <Utilities/NSObject+Utilities.h>
#import <Utilities/NSString+Utilities.h>
#import <Utilities/NSOperationQueue+Utilities.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Global Constants
#pragma mark -
//*********************************************************************************************************************//




StringConst(kLNSessionConnectivityChangedNotification);

StringConst(kLNSessionConnectivityKey);

StringConst(kLNSessionConnectivityGood);
StringConst(kLNSessionConnectivityModerate);
StringConst(kLNSessionConnectivityPoor);
StringConst(kLNSessionConnectivityNone);
StringConst(kLNSessionConnectivityDisconnected);

StringConst(kLNSessionJoinedUserNotification);
StringConst(kLNSessionLeftUserNotification);

StringConst(kLNSessionOngoingUserSynchronizeNotification);
StringConst(kLNSessionFailedUserSynchronizeNotification);
StringConst(kLNSessionFinishedUserSynchronizeNotification);

StringConst(kLNSessionSynchronizeProgressKey);
StringConst(kLNSessionSynchronizeFileKey);




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Node Description
#pragma mark -
//*********************************************************************************************************************//




@interface LNSessionNode : LNNode

@end

@implementation LNSessionNode

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LNSession
#pragma mark -
//*********************************************************************************************************************//




@interface LNSession () <LNBusManagerDelegate>
{
    NSMutableArray *_users;
}

@property (nonatomic, readonly  , strong) LNBusManager      *manager;

@property (nonatomic, readonly  , strong) NSString          *fileID;

@property (nonatomic, readwrite , strong) LNFileThumb       *thumb;
@property (nonatomic, readwrite , weak  ) id                thumbObserver;

@property (nonatomic, readwrite , assign) LNSessionType     type;

@property (nonatomic, readwrite , assign) LNSessionStatus   status;

@property (nonatomic, readwrite , strong) LNSelf            *me;

@property (nonatomic, readwrite , strong) LNUser            *host;

@property (nonatomic, readonly  , strong) NSString          *hostName;

@property (nonatomic, readonly  , strong) NSString          *userName;

@end

@implementation LNSession $remote(LNSessionNode)

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)createSessionWithManager:(LNBusManager *)manager metadata:(NSDictionary *)metadata
{
    return [[self alloc]
            initWithManager :manager
            announcement    :
            [NSString
             stringWithFormat   :@"_%@_%@_%@",
             [metadata[ @"ID"       ] readableRepresentation],
             [metadata[ @"name"     ] readableRepresentation],
             metadata[ @"fileID"   ]]
            metadata            :metadata];
}

+ (instancetype)createSessionWithManager:(LNBusManager *)manager announcement:(NSString *)announcement
{
    NSArray *components = [announcement componentsSeparatedByString:@"_"];

    if ([components count] != 4) {
        DBGError(@"Invalid announcement '%@'", announcement);
        return nil;
    }

    NSString *ID        = [NSString stringFromReadableRepresentation:components[1]];
    NSString *name      = [NSString stringFromReadableRepresentation:components[2]];
    NSString *fileID    = components[3];

    return [[self alloc]
            initWithManager :manager
            announcement    :announcement
            metadata            :
            (@{ @"ID"           :ID,
                @"name"         :name,
                @"fileID"       :fileID })];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)initWithNode:(LNNode *)node ID:(NSString *)ID __ILLEGALMETHOD__;

- (id)
initWithManager :(LNBusManager *)manager
announcement    :(NSString *    )announcement
metadata        :(NSDictionary *)metadata
{
    NSString *ID        = metadata[ @"ID"       ];
    NSString *name      = metadata[ @"name"     ];
    NSString *fileID    = metadata[ @"fileID"   ];

    if (!ID || !name || !fileID) {
        DBGError(@"Invalid metadata %@", metadata);
        return nil;
    }

    LNNode *node = [[LNSessionNode alloc] initWithManager:manager ID:ID];

    if ((self = [super initWithNode:node ID:ID])) {
        _announcement   = [announcement copy];
        _name           = [name copy];

        _fileID         = [fileID copy];

        _users          = [NSMutableArray new];

        [self.node.manager addDelegate:self];
    }

    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"<%@: %p, node: %@, name: %@, id: %@>",
            $classname,
            self,
            self.node,
            self.name,
            self.ID];
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic manager;

- (LNBusManager *)manager
{
    return [self.node manager];
}

@dynamic hostName;

- (NSString *)hostName
{
    return [self.manager hostNameOfSessionNamed:self.announcement];
}

@dynamic userName;

- (NSString *)userName
{
    return [self.manager peerName];
}

@dynamic users;

- (NSArray *)users
{
    return [_users copy];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)hostWithPassword:(NSString *)password
{
    if ([self type] == kLNSessionTypeUnknown) {
        LNSelf *me = [LNSelf selfWithSession:self];

        [me setPermissions:kLNUserPermissionsAdmin];
        [me setHost:YES];

        if ([self.manager createSessionNamed:self.announcement]) {
            [self setType:kLNSessionTypeHosted];
            [self setStatus:kLNSessionStatusActive];

            [self setMe:me];
            [self setHost:me];

            [self setPassword:password];

            [self initiateSynchronization];

            [$notifier postNotificationName:kLNetworkingSessionWasHostedNotification object:self];
        }
    } else {
        DBGError(@"Invalid type for session '%@'", self.name);
    }
}

- (void)joinWithPassword:(NSString *)password
{
    if ([self type] == kLNSessionTypeUnknown) {
        LNSelf *me      = [LNSelf selfWithSession:self];
        LNUser *host    = [LNUser userWithSession:self ID:self.hostName];

        [me setPermissions:kLNUserPermissionsRead];
        [host setHost:YES];

        if ([self.manager joinSessionNamed:self.announcement]) {
            [self setType:kLNSessionTypeRemote];
            [self setStatus:kLNSessionStatusActive];

            [self setMe:me];
            [self setHost:host];

            [self setPassword:password];

            [self select];

            [$notifier postNotificationName:kLNetworkingSessionWasJoinedNotification object:self];
        }
    } else {
        DBGError(@"Invalid type for session '%@'", self.name);
    }
}

- (void)select
{
    if ([self type] != kLNSessionTypeInvalid) {
        [$notifier postNotificationName:kLNetworkingSessionWasSelectedNotification object:self];
    } else {
        DBGError(@"Invalid session '%@'", self.name);
    }
}

- (void)leave
{
    switch ([self type]) {
        case kLNSessionTypeHosted: {
            [self setType:kLNSessionTypeInvalid];
            [self setStatus:kLNSessionStatusInactive];
        } break;
        case kLNSessionTypeRemote: {
            [self setType:kLNSessionTypeUnknown];
            [self setStatus:kLNSessionStatusUnknown];
        } break;
        default: {

        } return;
    }

    [self.manager leaveSessionNamed:self.announcement];

    [self->_users removeAllObjects];

    [self setMe:nil];
    [self setHost:nil];

    [$notifier postNotificationName:kLNetworkingSessionWasLeftNotification object:self];
}

- (void)generateThumbnail
{
    @weakify();
    void(^generateThumbnail)(void) = ^{
        @strongify();

        for (LNFile *file in [LNetworking files]) {
            if ([file.ID isEqualToString:self.fileID]) {
                [self setThumb:[LNFileThumb thumbWithFile:file]];
                break;
            }
        }
    };

    if (![self thumb]) {
        generateThumbnail();

        if (![self thumb]) {
            @weakify();
            [self setThumbObserver:
             [self observerForNotificationKey:kLNetworkingUpdatedFileAnnouncementNotification] (^{
                @strongify();

                generateThumbnail();

                if ([self thumb]) {
                    [self destroyNotificationObserver:self.thumbObserver];
                }
            })];
        }
    }
}

- (void)loadThumbnail:(void(^)(UIImage *))loadBlock
{
    if ([self thumb]) {
        [self.thumb loadImage:loadBlock];
    } else {
        @weakify();
        [self observerForKeyPath:@"thumb"] (^{
            @strongify();

            [self destroyObserversForKeyPath:@"thumb"];
            [self.thumb loadImage:loadBlock];
        });
    }
}

- (void)stopLoadThumbnail
{
    if ([self thumb]) {
        [self.thumb pauseLoadImage];
    } else {
        if ([self thumbObserver]) {
            [self destroyObserversForKeyPath:self.thumbObserver];
            [self destroyObserversForKeyPath:@"thumb"];
        }
    }
}

- (BOOL)isLinkedUser:(LNUser *)user
{
    NSNumber *isLinked = [user->_node associatedValueForKey:@selector(linkUser:)];

    return [isLinked boolValue];
}

- (void)linkUser:(LNUser *)user
{
    if (![self.me isHost]) {
        [user->_node linkToPeerNamed:user.ID inSessionNamed:self.announcement];
    }

    [user->_node.remote linkToPeerNamed:self.me.ID inSessionNamed:self.announcement];

    [user->_node setAssociatedValue:@YES forKey:@selector(linkUser:)];
}

- (void)unlinkUser:(LNUser *)user
{
    [user->_node setAssociatedValue:@NO forKey:@selector(linkUser:)];

    [user->_node.remote unlinkFromPeerNamed:self.me.ID inSessionNamed:self.announcement];

    if (![self.me isHost]) {
        [user->_node unlinkFromPeerNamed:user.ID inSessionNamed:self.announcement];
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)initializeFileWithCompletion:(void(^)(LNFile *))completion
{
    for (LNFile *announced in [LNetworking files]) {
        if ([announced.ID isEqualToString:self.fileID]) {
            completion(announced);
            return;
        }
    }

    DBGError(@"Cannot initialize with non-existent file ID '%@'", self.fileID);
}

- (void)initiateSynchronization
{
    @weakify();

    [self initializeFileWithCompletion:(^(LNFile *file) {
        @strongify();

        [file loadWithProgress:^(float progress) {
            [$notifier
             postNotificationName   :kLNSessionOngoingUserSynchronizeNotification
             object                 :[self me]
             userInfo               :@{kLNSessionSynchronizeFileKey      : file,
                                       kLNSessionSynchronizeProgressKey  : @(progress)}];
        } completion:^(BOOL finished) {
            if (finished) {
                [$notifier
                 postNotificationName   :kLNSessionFinishedUserSynchronizeNotification
                 object                 :[self me]
                 userInfo               :@{kLNSessionSynchronizeFileKey  : file}];
            } else {
                [self leave];

                [$notifier
                 postNotificationName   :kLNSessionFailedUserSynchronizeNotification
                 object                 :[self me]
                 userInfo               :@{kLNSessionSynchronizeFileKey  : file}];
            }
        } isPriority:YES];
    })];
}


#pragma mark - LNBusManagerDelegate
//*********************************************************************************************************************//

- (void)
busManager      :(LNBusManager *)manager
didAddPeerNamed :(NSString *    )peerName
toSessionNamed  :(NSString *    )sessionName
{
    @weakify();
    [$mainqueue addSerialOperationWithBlock:^{
        @strongify();

        LNUser *user;

        if (![self.me.ID isEqualToString:peerName]) {
            if ([self.host.ID isEqualToString:peerName]) {
                user = [self host];

                [user->_node linkToPeerNamed:peerName inSessionNamed:self.announcement];

                [self.manager startMonitoringConnectivityToPeerNamed:peerName withBlock:

                 ^(LNBusManagerConnectivity connectivity) {
                     [[NSOperationQueue mainQueue] addSerialOperationWithBlock:^{
                         NSString *state;

                         switch (connectivity) {
                             case kLNBusManagerConnectivityGood: {
                                 state = kLNSessionConnectivityGood;
                             } break;
                             case kLNBusManagerConnectivityModerate: {
                                 state = kLNSessionConnectivityModerate;
                             } break;
                             case kLNBusManagerConnectivityPoor: {
                                 state = kLNSessionConnectivityPoor;
                             } break;
                             case kLNBusManagerConnectivityNone: {
                                 state = kLNSessionConnectivityNone;
                             } break;
                             default: {
                                 state = kLNSessionConnectivityDisconnected;
                             } break;
                         }

                         [$notifier
                          postNotificationName  :kLNSessionConnectivityChangedNotification
                          object                :self
                          userInfo              :@{kLNSessionConnectivityKey: state}];
                     }];
                 }
                 
                 ];

                [self initiateSynchronization];
            } else {
                [self->_users addObject:(

                    user = [LNUser userWithSession:self ID:peerName]

                 )];

                if ([self.me isHost]) {
                    [user->_node linkToPeerNamed:peerName inSessionNamed:self.announcement];
                    [self.me->_node linkToPeerNamed:peerName inSessionNamed:self.announcement];
                }
            }

            [$notifier postNotificationName:kLNSessionJoinedUserNotification object:user];
        } else {
            user = [self me];
        }

        DBGInformation(@"User %@ joined", user);
    }];
}

- (void)
busManager          :(LNBusManager *)manager
didRemovePeerNamed  :(NSString *    )peerName
fromSessionNamed    :(NSString *    )sessionName
{
    @weakify();
    [$mainqueue addSerialOperationWithBlock:^{
        @strongify();

        [self.manager stopMonitoringConnectivityToPeerNamed:peerName];
        [self.me->_node unlinkFromPeerNamed:peerName inSessionNamed:sessionName];

        NSUInteger index
        =
        [self.users indexOfObjectPassingTest:
         ^(LNUser *user, NSUInteger index, BOOL *stop) {
             return (*stop = [user.ID isEqualToString:peerName]);
         }];
        
        if (index != NSNotFound) {
            LNUser *user = self.users[index];
            [self->_users removeObjectAtIndex:index];
            
            [$notifier postNotificationName:kLNSessionLeftUserNotification object:user];
            
            DBGInformation(@"User %@ left", user);
        }
    }];
}

@end
