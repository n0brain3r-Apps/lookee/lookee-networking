//
//  LNBusManager.m
//  Writeability
//
//  Created by Ryan on 8/18/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNBusManager.h"

#import "LNBusObject.h"

#import "AllJoynFramework/AJNBusAttachment.h"
#import "AllJoynFramework/AJNProxyBusObject.h"
#import "AllJoynFramework/AJNBusObject.h"

#import <Utilities/LMacros.h>
#import <Utilities/LGroupProxy.h>
#import <Utilities/LQueuingProxy.h>
#import <Utilities/LTimer.h>

#import <Utilities/NSObject+Utilities.h>
#import <Utilities/NSString+Utilities.h>
#import <Utilities/NSThread+Block.h>





//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//





@interface LNSessionDescriptor : NSObject

@property (nonatomic, readwrite , copy  ) NSString          *advertisement;

@property (nonatomic, readonly  , strong) NSMutableSet      *peerNames;

@property (nonatomic, readwrite , strong) NSString          *hostName;

@property (nonatomic, readwrite , copy  ) NSString          *name;

@property (nonatomic, readwrite , assign) AJNSessionPort    port;

@property (nonatomic, readwrite , assign) AJNSessionId      ID;

@end


@interface LNPeerConnectivityDescriptor : NSObject

@property (nonatomic, readwrite, assign) LNBusManagerConnectivity   connectivity;

@property (nonatomic, readwrite, strong) NSString                   *peerName;

@property (nonatomic, readwrite, strong) void                       (^block)(LNBusManagerConnectivity);

@property (nonatomic, readwrite, assign) BOOL                       shouldMonitor;

@end





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LNBusManager
#pragma mark -
//*********************************************************************************************************************//





@interface LNBusManager () <AJNBusListener, AJNSessionListener, AJNSessionPortListener>

@property (nonatomic, readonly  , strong) AJNBusAttachment      *bus;

@property (nonatomic, readonly  , strong) AJNSessionOptions     *sessionOptions;

@property (nonatomic, readonly  , strong) NSMutableDictionary   *discoveredSessions;

@property (nonatomic, readonly  , strong) NSMutableDictionary   *hostedSessions;

@property (nonatomic, readonly  , strong) NSMutableDictionary   *joinedSessions;

@property (nonatomic, readonly  , strong) NSMutableDictionary   *banishedPeerGUIDs;

@property (nonatomic, readonly  , strong) LGroupProxy           <LNBusManagerDelegate>*delegates;

@end

@implementation LNBusManager

#pragma mark - Static Objects
//*********************************************************************************************************************//

static uint32_t const kLNBusManagerDefaultSessionTimeout = 5;


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)init __ILLEGALMETHOD__;

- (id)initWithDomainName:(NSString *)domainName
{
    if ((self = [super init])) {
        _domainName             = [domainName copy];

        _bus                    = [[AJNBusAttachment alloc]
                                   initWithApplicationName      :self.domainName
                                   allowRemoteMessages          :YES
                                   maximumConcurrentOperations  :4];

        _sessionOptions         = [[AJNSessionOptions alloc]
                                   initWithTrafficType  :kAJNTrafficMessages
                                   supportsMultipoint   :YES
                                   proximity            :kAJNProximityAny
                                   transportMask        :kAJNTransportMaskAny];

        _discoveredSessions     = [LQueuingProxy proxyWithObject:[NSMutableDictionary new]];
        _hostedSessions         = [LQueuingProxy proxyWithObject:[NSMutableDictionary new]];
        _joinedSessions         = [LQueuingProxy proxyWithObject:[NSMutableDictionary new]];

        _banishedPeerGUIDs      = [LQueuingProxy proxyWithObject:[NSMutableDictionary new]];

        _delegates              = [LGroupProxy proxy];

        if (![self initialize]) {
            return nil;
        }
    }

    return self;
}

- (BOOL)initialize
{
    QStatus status;

#if DEBUG
    [self.bus setDaemonDebugLevel:7 forModule:@"ALL"];
#else
    [self.bus setDaemonDebugLevel:0 forModule:@"ALL"];
#endif

    [self.bus registerBusListener:self];

    status = [self.bus start];

    if (status == ER_OK) {
        status = [self.bus connectWithArguments:@"null:"];

        if (status == ER_OK) {
            status = [self.bus findAdvertisedName:self.domainName];

            if (status == ER_OK) {
                return YES;
            } else {
                DBGWarning(@"Failed discovery with bus with domain '%@' (%s)", self.domainName, QCC_StatusText(status));
            }
        } else {
            DBGWarning(@"Failed to connect bus with domain '%@' (%s)", self.domainName, QCC_StatusText(status));
        }
    } else {
        DBGWarning(@"Failed to start bus with domain '%@' (%s)", self.domainName, QCC_StatusText(status));
    }

    return NO;
}

- (void)dealloc
{
    [self cleanup];
}

- (void)cleanup
{
    [self enumerateSessionDescriptors:^(LNSessionDescriptor *descriptor) {
        [self leaveSessionNamed:descriptor.name];

        return NO;
    }];

    [self.bus unregisterBusListener:self];

    QStatus status = [self.bus cancelFindAdvertisedName:self.domainName];

    if (status != ER_OK) {
        DBGWarning(@"Failed to cancel discovery (%s)", QCC_StatusText(status));
    }

    status = [self.bus disconnectWithArguments:@"null:"];

    if (status != ER_OK) {
        DBGWarning(@"Failed to disconnect bus (%s)", QCC_StatusText(status));
    }

    status = [self.bus stop];

    if (status != ER_OK) {
        DBGWarning(@"Failed to stop bus (%s)", QCC_StatusText(status));
    }

    status = [self.bus waitUntilStopCompleted];

    if (status != ER_OK) {
        DBGWarning(@"Failed to wait until bus stopped (%s)", QCC_StatusText(status));
    }
    
    [self.bus destroy];
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic discoveredSessionNames;

- (NSArray *)discoveredSessionNames
{
    return [self.discoveredSessions allKeys];
}

@dynamic hostedSessionNames;

- (NSArray *)hostedSessionNames
{
    return [self.hostedSessions allKeys];
}

@dynamic joinedSessionNames;

- (NSArray *)joinedSessionNames
{
    return [self.joinedSessions allKeys];
}

@dynamic GUID;

- (NSString *)GUID
{
    return [self.bus uniqueIdentifier];
}

@dynamic peerName;

- (NSString *)peerName
{
    return [self.bus uniqueName];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)addDelegate:(id<LNBusManagerDelegate>)delegate
{
    [self.delegates $addObject:delegate];
}

- (void)removeDelegate:(id<LNBusManagerDelegate>)delegate
{
    [self.delegates $removeObject:delegate];
}

- (void)registerBusModule:(id<LNBusManagerModule>)module toSessionNamed:(NSString *)sessionName
{
    NSParameterAssert([sessionName length] != 0);

    LNSessionDescriptor *descriptor = [self descriptorForSessionNamed:sessionName];

    if (descriptor) {
        [module registerOnBus:self.bus withSessionIdentifier:descriptor.ID];
    } else {
        DBGError(@"Session named '%@' does not exist", sessionName);
    }
}

- (void)registerBusObject:(LNBusObject<LNBusObjectDelegate>*)busObject
{
    if([busObject respondsToSelector:@selector(registerInterfacesWithBus:)]) {
        [busObject performSelector:@selector(registerInterfacesWithBus:) withObject:self.bus];
    }

    QStatus status = [self.bus registerBusObject:busObject];

    if (status != ER_OK) {
        DBGError(@"Failed to register bus object %@ (%s)", busObject, QCC_StatusText(status));
    }
}

- (void)unregisterBusObject:(LNBusObject<LNBusObjectDelegate> *)busObject
{
    [self.bus unregisterBusObject:busObject];
}

- (id)
remoteObjectWithType:(Class     )type
withPath            :(NSString *)path
atPeerNamed         :(NSString *)peerName
inSessionNamed      :(NSString *)sessionName
{
    LNSessionDescriptor *descriptor = [self descriptorForSessionNamed:sessionName];

    if (descriptor) {
        return [(AJNProxyBusObject *)
                [type alloc]
                initWithBusAttachment   :self.bus
                serviceName             :peerName
                objectPath              :path
                sessionId               :descriptor.ID];
    } else {
        DBGWarning(@"Session named '%@' does not exist", sessionName);
    }

    return nil;
}

- (NSString *)sessionNameForRemoteObject:(id)object
{
    AJNProxyBusObject *proxy = object;

    return [self descriptorForSessionWithIdentifier:proxy.sessionId].name;
}

- (BOOL)createSessionNamed:(NSString *)sessionName
{
    NSParameterAssert([sessionName length] != 0);

    AJNSessionPort sessionPort = [self.bus bindSessionOnAnyPortWithOptions:self.sessionOptions withDelegate:self];

    if (sessionPort != kAJNSessionPortAny) {
        if (![self descriptorForSessionNamed:sessionName]) {
            NSString *advertisement = [self advertisementWithSessionName:sessionName andSessionPort:sessionPort];

            QStatus status = [self.bus requestWellKnownName:advertisement withFlags:kAJNBusNameFlagDoNotQueue];

            if (status == ER_OK) {
                status = [self.bus advertiseName:advertisement withTransportMask:self.sessionOptions.transports];

                if (status == ER_OK) {
                    LNSessionDescriptor *descriptor = [LNSessionDescriptor new];
                    [descriptor setPort:sessionPort];

                    [self.hostedSessions setObject:descriptor forKey:sessionName];

                    return YES;
                } else {
                    DBGWarning(@"Failed to advertise session named '%@' (%s)", sessionName, QCC_StatusText(status));
                }

                status = [self.bus releaseWellKnownName:advertisement];

                if (status != ER_OK) {
                    DBGWarning(@"Failed to release session named '%@' (%s)", sessionName, QCC_StatusText(status));
                }
            } else {
                DBGWarning(@"Failed to request session named '%@' (%s)", sessionName, QCC_StatusText(status));
            }
        } else {
            DBGWarning(@"Session named '%@' already exists", sessionName);
        }
        
        [self.bus unbindSessionFromPort:sessionPort];
    } else {
        DBGWarning(@"Failed to bind session '%@' to a port", sessionName);
    }

    return NO;
}

- (BOOL)renameSessionNamed:(NSString *)sessionName toName:(NSString *)name
{
    NSParameterAssert([sessionName length] != 0 && [name length] != 0);

    LNSessionDescriptor *descriptor = self.hostedSessions[sessionName];

    if (descriptor) {
        if (![self descriptorForSessionNamed:name]) {
            NSString *advertisement = [self advertisementWithSessionName:sessionName andSessionPort:descriptor.port];

            QStatus status = [self.bus requestWellKnownName:advertisement withFlags:kAJNBusNameFlagDoNotQueue];

            if (status == ER_OK) {
                status = [self.bus advertiseName:advertisement withTransportMask:self.sessionOptions.transports];

                if (status == ER_OK) {
                    [descriptor setName:name];

                    [self.hostedSessions setObject:descriptor forKey:name];

                    status = [[self bus]
                              cancelAdvertisedName  :descriptor.advertisement
                              withTransportMask     :self.sessionOptions.transports];

                    if (status == ER_OK) {
                        [self.hostedSessions removeObjectForKey:sessionName];

                        return YES;
                    } else {
                        DBGWarning(@"Failed to cancel session named '%@' (%s)", sessionName, QCC_StatusText(status));
                    }
                } else {
                    DBGWarning(@"Failed to advertise session named '%@' (%s)", name, QCC_StatusText(status));
                }

                status = [self.bus releaseWellKnownName:advertisement];

                if (status != ER_OK) {
                    DBGWarning(@"Failed to release session named '%@' (%s)", sessionName, QCC_StatusText(status));
                }
            } else {
                DBGWarning(@"Failed to request session named '%@' (%s)", name, QCC_StatusText(status));
            }
        } else {
            DBGWarning(@"Session named '%@' already exists", name);
        }
    } else {
        DBGWarning(@"Session named '%@' is not being hosted", name);
    }

    return NO;
}

- (BOOL)joinSessionNamed:(NSString *)sessionName
{
    NSParameterAssert([sessionName length] != 0);

    LNSessionDescriptor *descriptor = self.discoveredSessions[sessionName];

    if (descriptor) {
        if ([descriptor port]) {
            AJNSessionId identifier
            =
            [[self bus]
             joinSessionWithName:[descriptor advertisement]
             onPort             :[descriptor port]
             withDelegate       :self
             options            :[self sessionOptions]];

            if (identifier > 0) {
                [descriptor setID:identifier];

                [self.joinedSessions setObject:descriptor forKey:sessionName];

                [self setTimeoutInSeconds:kLNBusManagerDefaultSessionTimeout forSessionWithName:sessionName];

                return YES;
            } else {
                DBGWarning(@"Failed to join session named '%@'", sessionName);
            }
        }
    } else {
        DBGWarning(@"Session named '%@' has not been discovered yet", sessionName);
    }

    return NO;
}

- (BOOL)leaveSessionNamed:(NSString *)sessionName
{
    NSParameterAssert([sessionName length] != 0);

    LNSessionDescriptor *descriptor = self.hostedSessions[sessionName];

    QStatus status;

    if (descriptor) {
        status
        =
        [[self bus]
         cancelAdvertisedName   :[descriptor advertisement]
         withTransportMask      :[self.sessionOptions transports]];

        if (status == ER_OK) {
            status = [self.bus releaseWellKnownName:descriptor.advertisement];

            if (status == ER_OK) {
                status = [self.bus unbindSessionFromPort:descriptor.port];

                if (status == ER_OK) {
                    for (NSString *peerName in descriptor.peerNames.copy) {
                        [self didRemoveMemberNamed:peerName fromSession:descriptor.ID];
                    }

                    [self didRemoveMemberNamed:self.peerName fromSession:descriptor.ID];

                    [self.hostedSessions removeObjectForKey:sessionName];

                    return YES;
                } else {
                    DBGWarning(@"Failed to unbind session named '%@' (%s)", sessionName, QCC_StatusText(status));

                    status
                    =
                    [[self bus]
                     requestWellKnownName   :[descriptor advertisement]
                     withFlags              :kAJNBusNameFlagDoNotQueue];

                    if (status != ER_OK) {
                        DBGWarning(@"Failed to re-request session named '%@' (%s)", sessionName, QCC_StatusText(status));
                    }
                }
            } else {
                DBGWarning(@"Failed to release session named '%@' (%s)", sessionName, QCC_StatusText(status));
            }

            status
            =
            [[self bus]
             advertiseName      :[descriptor advertisement]
             withTransportMask  :[self.sessionOptions transports]];

            if (status != ER_OK) {
                DBGWarning(@"Failed to re-advertise session named '%@' (%s)", sessionName, QCC_StatusText(status));
            }
        } else {
            DBGWarning(@"Failed to cancel session named '%@' (%s)", sessionName, QCC_StatusText(status));
        }
    } else {
        descriptor = self.joinedSessions[sessionName];

        status = [self.bus leaveSession:descriptor.ID];

        if (status == ER_OK) {
            for (NSString *peerName in descriptor.peerNames.copy) {
                [self didRemoveMemberNamed:peerName fromSession:descriptor.ID];
            }

            [self didRemoveMemberNamed:self.peerName fromSession:descriptor.ID];

            [self.joinedSessions removeObjectForKey:sessionName];

            return YES;
        } else {
            DBGWarning(@"Failed to leave session named '%@' (%s)", sessionName, QCC_StatusText(status));
        }
    }

    return NO;
}

- (void)setTimeoutInSeconds:(uint32_t)timeout forSessionWithName:(NSString *)sessionName
{
    LNSessionDescriptor *descriptor = self.joinedSessions[sessionName];

    if (descriptor) {
        QStatus status
        =
        [[self bus]
         setLinkTimeoutAsync:timeout
         forSession         :[descriptor ID]
         completionBlock    :
         ^(QStatus status, uint32_t timeout, void *context) {
             if (status != ER_OK) {
                 DBGWarning(@"Failed to set timeout (%u) for session named '%@' (%s)",
                            timeout, sessionName, QCC_StatusText(status));
             }
         }
         context            :NULL];

        if (status != ER_OK) {
            DBGWarning(@"Failed to set timeout (%u) for session named '%@' (%s)",
                       timeout, sessionName, QCC_StatusText(status));
        }
    } else {
        DBGError(@"Session named '%@' has not been joined", sessionName);
    }
}

- (NSString *)hostNameOfSessionNamed:(NSString *)sessionName
{
    return [self descriptorForSessionNamed:sessionName].hostName;
}

- (NSArray *)peerNamesInSessionNamed:(NSString *)sessionName
{
    return [[self descriptorForSessionNamed:sessionName].peerNames allObjects];
}

- (BOOL)isReachablePeerNamed:(NSString *)peerName forTimeout:(uint32_t)timeout
{
    NSParameterAssert([peerName length] != 0);

    QStatus status = [self.bus pingPeer:peerName withTimeout:timeout];

    if (status == ER_OK) {
        return YES;
    } else {
        DBGInformation(@"Peer with name '%@' is not reachable (%s)", peerName, QCC_StatusText(status));
    }

    return NO;
}

- (void)getConnectivityToPeerNamed:(NSString *)peerName withBlock:(void(^)(LNBusManagerConnectivity))block
{
    DBGParameterAssert((peerName != nil) && (block != nil));

    LNPeerConnectivityDescriptor *descriptor = [LNPeerConnectivityDescriptor new];
    [descriptor setPeerName:[peerName copy]];
    [descriptor setBlock:block];

    [self getConnectivityToPeerWithDescriptor:descriptor];
}

- (void)startMonitoringConnectivityToPeerNamed:(NSString *)peerName withBlock:(void(^)(LNBusManagerConnectivity))block
{
    DBGParameterAssert((peerName != nil) && (block != nil));

    LNPeerConnectivityDescriptor *descriptor = [LNPeerConnectivityDescriptor new];
    [descriptor setShouldMonitor:YES];
    [descriptor setPeerName:[peerName copy]];
    [descriptor setBlock:block];

    [self performSelector:@selector(getConnectivityToPeerWithDescriptor:) withObject:descriptor afterDelay:1.];
}

- (void)stopMonitoringConnectivityToPeerNamed:(NSString *)peerName
{
    DBGParameterAssert(peerName != nil);

    LNPeerConnectivityDescriptor *descriptor = [LNPeerConnectivityDescriptor new];
    [descriptor setPeerName:[peerName copy]];

    [NSObject
     cancelPreviousPerformRequestsWithTarget:self
     selector                               :@selector(getConnectivityToPeerWithDescriptor:)
     object                                 :descriptor];
}

- (BOOL)removePeerNamed:(NSString *)peerName fromSessionNamed:(NSString *)sessionName
{
    NSParameterAssert([peerName length] != 0 && [sessionName length] != 0);

    LNSessionDescriptor *descriptor = self.hostedSessions[sessionName];

    return( [self.bus removeSessionMember:descriptor.ID withName:peerName] == ER_OK );
}

- (void)banishPeerNamed:(NSString *)peerName fromSessionNamed:(NSString *)sessionName
{
    NSParameterAssert([peerName length] != 0 && [sessionName length] != 0);

    NSString *peerGUID = [self.bus guidForPeerNamed:peerName];

    if (peerGUID) {
        if ([self removePeerNamed:peerName fromSessionNamed:sessionName]) {
            NSMutableSet *peerGUIDs = self.banishedPeerGUIDs[sessionName];

            if (!peerGUIDs) {
                peerGUIDs = [LQueuingProxy proxyWithObject:[NSMutableSet new]];
            }

            [peerGUIDs addObject:peerGUID];
        }
    } else {
        DBGWarning(@"Failed to obtain GUID for peer named '%@'", peerName);
    }
}

- (void)permitPeerNamed:(NSString *)peerName toSessionNamed:(NSString *)sessionName
{
    NSParameterAssert([peerName length] != 0 && [sessionName length] != 0);

    NSString *peerGUID = [self.bus guidForPeerNamed:peerName];

    if (peerGUID) {
        [self.banishedPeerGUIDs[sessionName] removeObject:peerGUID];
    } else {
        DBGWarning(@"Failed to obtain GUID for peer named '%@'", peerName);
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (NSString *)advertisementWithSessionName:(NSString *)sessionName andSessionPort:(AJNSessionPort)port
{
    return [[self domainName]
            stringByAppendingString:
            [NSString stringWithFormat:
             @"._%@_%@_%u",
             self.peerName.readableRepresentation,
             sessionName,
             port]];
}

- (NSDictionary *)parseAdvertisement:(NSString *)advertisement
{
    NSError *error = nil;

    NSString *pattern = [NSString stringWithFormat:@"%@._([A-Za-z0-9]+)_(\\w+)_(\\d+)", self.domainName];

    NSRegularExpression *scanner =
    [NSRegularExpression
     regularExpressionWithPattern:pattern
     options                     :0
     error                       :&error];

    if (!error) {
        NSTextCheckingResult *match
        =
        [scanner
         firstMatchInString :advertisement
         options            :0
         range              :
         ((NSRange) {
            .location   = 0,
            .length     = [advertisement length]
        })];

        NSString *hostRepresentation = [advertisement substringWithRange:[match rangeAtIndex:1]];

        return
        (@{ @"advertisement": [advertisement copy],
            @"hostName"     : [NSString stringFromReadableRepresentation:hostRepresentation],
            @"name"         : [advertisement substringWithRange:[match rangeAtIndex:2]],
            @"port"         : @([advertisement substringWithRange:[match rangeAtIndex:3]].intValue)
           });
    }

    return nil;
}

- (void)enumerateSessionDescriptors:(BOOL(^)(LNSessionDescriptor *descriptor))block
{
    for (LNSessionDescriptor *descriptor in self.discoveredSessions.allValues) {
        if (block(descriptor)) {
            return;
        }
    }

    for (LNSessionDescriptor *descriptor in self.hostedSessions.allValues) {
        if (block(descriptor)) {
            return;
        }
    }

    for (LNSessionDescriptor *descriptor in self.joinedSessions.allValues) {
        if (block(descriptor)) {
            return;
        }
    }
}

- (void)getConnectivityToPeerWithDescriptor:(LNPeerConnectivityDescriptor *)descriptor
{
    NSThread *thread = $thisthread;

    [NSThread performBlockInBackground:^{
        NSDate          *start      = [NSDate new];

        QStatus         status      = [self.bus pingPeer:descriptor.peerName withTimeout:8000];

        NSTimeInterval  interval    = [[NSDate new] timeIntervalSinceDate:start];

        LNBusManagerConnectivity connectivity;

        switch (status) {
            case ER_OK: {
                if (islessequal(interval, 1.)) {
                    connectivity = kLNBusManagerConnectivityGood;
                } else if (islessequal(interval, 2.)) {
                    connectivity = kLNBusManagerConnectivityModerate;
                } else if (islessequal(interval, 4.)) {
                    connectivity = kLNBusManagerConnectivityPoor;
                } else {
                    connectivity = kLNBusManagerConnectivityNone;
                }
            } break;
            default: {
                connectivity = kLNBusManagerConnectivityDisconnected;
            } break;
        }

        if ([descriptor shouldMonitor]) {
            if ([descriptor connectivity] != connectivity) {
                [descriptor block](connectivity);
            }

            [thread performBlock:^{
                [self performSelector:_cmd withObject:descriptor afterDelay:1.];
            } waitUntilDone:YES];
        } else {
            [descriptor block](connectivity);
        }

        [descriptor setConnectivity:connectivity];
    }];
}

- (LNSessionDescriptor *)descriptorForSessionNamed:(NSString *)sessionName
{
    NSParameterAssert([sessionName length] != 0);

    return( self.hostedSessions[sessionName]?:self.discoveredSessions[sessionName] );
}

- (LNSessionDescriptor *)descriptorForSessionWithPort:(AJNSessionId)port
{
    LNSessionDescriptor *__block found;

    [self enumerateSessionDescriptors:^(LNSessionDescriptor *descriptor) {
        if ([descriptor port] == port) {
            found = descriptor;
            return YES;
        }

        return NO;
    }];

    return found;
}

- (LNSessionDescriptor *)descriptorForSessionWithIdentifier:(AJNSessionId)identifier
{
    LNSessionDescriptor *__block found;

    [self enumerateSessionDescriptors:^(LNSessionDescriptor *descriptor) {
        if ([descriptor ID] == identifier) {
            found = descriptor;
            return YES;
        }

        return NO;
    }];

    return found;
}


#pragma mark - AJNBusListener
//*********************************************************************************************************************//

- (void)
didFindAdvertisedName   :(NSString *        )advertisement
withTransportMask       :(AJNTransportMask  )transport
namePrefix              :(NSString *        )namePrefix
{
    NSDictionary *parsed = [self parseAdvertisement:advertisement];

    if (parsed) {
        NSString *sessionName = parsed[@"name"];

        LNSessionDescriptor *descriptor = self.hostedSessions[sessionName];

        if (!descriptor) {
            descriptor = self.discoveredSessions[sessionName];

            if (!descriptor) {
                descriptor = [LNSessionDescriptor new];

                [descriptor setValuesForKeysWithDictionary:parsed];
                [self.discoveredSessions setObject:descriptor forKey:sessionName];

                if ([self.delegates respondsToSelector:@selector(busManager:didAddSessionNamed:)]) {
                    [self.delegates busManager:self didAddSessionNamed:sessionName];
                }

                return;
            }
        }

        [descriptor setValuesForKeysWithDictionary:parsed];
    } else {
        DBGWarning(@"Found session with invalid advertisement '%@'", advertisement);
    }
}

- (void)
didLoseAdvertisedName   :(NSString *        )advertisement
withTransportMask       :(AJNTransportMask  )transport
namePrefix              :(NSString *        )namePrefix
{
    NSDictionary *parsed = [self parseAdvertisement:advertisement];

    if (parsed) {
        NSString *sessionName = parsed[@"name"];

        if (self.discoveredSessions[sessionName]) {
            if ([self.delegates respondsToSelector:@selector(busManager:didRemoveSessionNamed:)]) {
                [self.delegates busManager:self didRemoveSessionNamed:sessionName];
            }

            [self.discoveredSessions removeObjectForKey:sessionName];
        }
    } else {
        DBGWarning(@"Lost session with invalid advertisement '%@'", advertisement);
    }
}

- (void)nameOwnerChanged:(NSString *)name to:(NSString *)newOwner from:(NSString *)previousOwner;
{
    if (previousOwner) {
        LNSessionDescriptor *descriptor = [self descriptorForSessionNamed:name];

        if (descriptor) {
            [descriptor setHostName:newOwner];

            if ([descriptor.peerNames containsObject:previousOwner]) {
                [descriptor.peerNames removeObject:previousOwner];
                [descriptor.peerNames addObject:newOwner];
            }
        }
    }
}


#pragma mark - AJNSessionListener
//*********************************************************************************************************************//

- (void)didAddMemberNamed:(NSString *)peerName toSession:(AJNSessionId)identifier
{
    LNSessionDescriptor *descriptor = [self descriptorForSessionWithIdentifier:identifier];

    if (descriptor) {
        NSArray *peerNames = @[[self peerName], peerName];

        for (NSString *peerName in peerNames) {
            if (![descriptor.peerNames containsObject:peerName]) {
                [descriptor.peerNames addObject:peerName];

                if ([self.delegates respondsToSelector:@selector(busManager:didAddPeerNamed:toSessionNamed:)]) {
                    [self.delegates busManager:self didAddPeerNamed:peerName toSessionNamed:descriptor.name];
                }
            }
        }
    } else {
        DBGWarning(@"Could not add a member for session identifier %u", identifier);
    }
}

- (void)didRemoveMemberNamed:(NSString *)peerName fromSession:(AJNSessionId)identifier
{
    LNSessionDescriptor *descriptor = [self descriptorForSessionWithIdentifier:identifier];

    if (descriptor) {
        if ([descriptor.peerNames containsObject:peerName]) {
            if ([self.delegates respondsToSelector:@selector(busManager:didRemovePeerNamed:fromSessionNamed:)]) {
                [self.delegates busManager:self didRemovePeerNamed:peerName fromSessionNamed:descriptor.name];
            }

            [descriptor.peerNames removeObject:peerName];
        }
    } else {
        DBGWarning(@"Could not remove a member for session identifier %u", identifier);
    }
}

- (void)sessionWasLost:(AJNSessionId)identifier
{
    LNSessionDescriptor *descriptor = [self descriptorForSessionWithIdentifier:identifier];

    if (descriptor) {
        if ([self.delegates respondsToSelector:@selector(busManager:didLoseSessionNamed:)]) {
            [self.delegates busManager:self didLoseSessionNamed:descriptor.name];
        }
    } else {
        DBGWarning(@"Lost session with invalid identifier %u", identifier);
    }
}


#pragma mark - AJNSessionPortListener
//*********************************************************************************************************************//

- (BOOL)
shouldAcceptSessionJoinerNamed  :(NSString *            )peerName
onSessionPort                   :(AJNSessionPort        )port
withSessionOptions              :(AJNSessionOptions *   )options
{
    LNSessionDescriptor *descriptor = [self descriptorForSessionWithPort:port];
    NSString            *peerGUID   = [self.bus guidForPeerNamed:peerName];

    NSMutableSet        *peerGUIDs  = [self banishedPeerGUIDs][descriptor.name];

    return( !peerGUID || ![peerGUIDs containsObject:peerGUID] );
}

- (void)
didJoin         :(NSString *    )peerName
inSessionWithId :(AJNSessionId  )identifier
onSessionPort   :(AJNSessionPort)port
{
    [self.bus bindSessionListener:self toSession:identifier];

    LNSessionDescriptor *descriptor = [self descriptorForSessionWithPort:port];

    if (descriptor) {
        [descriptor setID:identifier];

        [self didAddMemberNamed:peerName toSession:identifier];
    } else {
        DBGWarning(@"Could not add a member for session identifier %u", identifier);
    }
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LNSessionDescriptor

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (id)init
{
    if ((self = [super init])) {
        _peerNames = [LQueuingProxy proxyWithObject:[NSMutableSet new]];
    }

    return self;
}

@end


@implementation LNPeerConnectivityDescriptor

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[self class]]) {
        return [self.peerName isEqualToString:[(LNPeerConnectivityDescriptor *)object peerName]];
    }

    return [super isEqual:object];
}

@end