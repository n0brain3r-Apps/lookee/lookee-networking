//
//  LNFile.h
//  Writeability
//
//  Created by Ryan on 5/24/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface LNFile : NSObject

@property (atomic, readonly, assign, getter = isOwned       ) BOOL owned;
@property (atomic, readonly, assign, getter = isTransferring) BOOL transferring;

@property (nonatomic, readonly, strong) NSOrderedSet    *ownerIDs;
@property (nonatomic, readonly, strong) NSString        *path;
@property (nonatomic, readonly, strong) NSString        *name;
@property (nonatomic, readonly, strong) NSString        *ID;
@property (nonatomic, readonly, strong) NSData          *sum;
@property (nonatomic, readonly, assign) NSUInteger      size;
@property (nonatomic, readonly, strong) NSDate          *dateCreated;


- (void)loadWithProgress:(void(^)(float))progress completion:(void(^)(BOOL))completion isPriority:(BOOL)priority;
- (void)pauseLoad;
- (void)cancelLoad;

@end