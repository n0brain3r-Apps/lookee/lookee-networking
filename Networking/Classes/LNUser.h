//
//  LParticipant.h
//  Writeability
//
//  Created by Ryan on 5/25/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LRemoteList.h"



typedef NS_OPTIONS(NSUInteger, LNUserPermissions)
{
    kLNUserPermissionsRead       = 0 << 0,
    kLNUserPermissionsWrite      = 1 << 0,
    kLNUserPermissionsPresent    = 1 << 1,
    kLNUserPermissionsAdmin      = 1 << 2
};




@interface LNUser : LNRemote

@property (nonatomic, readonly  , assign, getter=isHost     ) BOOL host;
@property (nonatomic, readwrite , assign, getter=isLocked   ) BOOL locked;

@property (nonatomic, readwrite , assign) LNUserPermissions permissions;

@property (nonatomic, readonly  , strong) NSString          *name;

@property (nonatomic, readonly  , strong) LRemoteList       *datapoints;

@end

@interface LNSelf : LNUser

@property (nonatomic, readwrite , strong) NSString      *name;

@property (nonatomic, readonly  , strong) LRemoteArray  *datapoints;

@end