//
//  LRemoteObject.h
//  Writeability
//
//  Created by Ryan on 6/26/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>


@protocol LRemoteObject <NSObject>
@required

- (id)initWithStringRepresentation:(NSString *)representation;

- (NSString *)stringRepresentation;

- (BOOL)isValid;

@optional

- (Class)resolveType;

@end
