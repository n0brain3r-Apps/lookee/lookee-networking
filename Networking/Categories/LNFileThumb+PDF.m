//
//  LNFileThumb+PDF.m
//  Writeability
//
//  Created by Ryan on 8/15/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNFileThumb+PDF.h"

#import <Utilities/LMacros.h>
#import <Utilities/LPDF.h>





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LNFileThumb
#pragma mark -
//*********************************************************************************************************************//





@implementation LNFileThumb (PDF)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (UIImage *)generateImageFromPDFFileAtPath:(NSString *)path
{
    CGSize size = $this.size;

    return LPDFPageSnapshot([NSURL fileURLWithPath:path], 1, size.width, size.height, 0.);
}

@end
