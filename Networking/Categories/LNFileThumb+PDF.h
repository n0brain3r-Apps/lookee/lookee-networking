//
//  LNFileThumb+PDF.h
//  Writeability
//
//  Created by Ryan on 8/15/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LNFileThumb.h"


@interface LNFileThumb (PDF)

- (UIImage *)generateImageFromPDFFileAtPath:(NSString *)path;

@end
